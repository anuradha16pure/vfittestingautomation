/** UI TEST VFIT */

const expect = require('chai').expect;
CodiceFiscale = require('codice-fiscale-js/dist/codice.fiscale.commonjs2').CodiceFiscale

describe('Flow Test', function () {

    
    ///common functions
    function randomText() {
        var text = "";
        //Here vowels are taken into possible just to make the generation of fiscal code easier.
        var possible = "BCDFGHJKLMNPQRSTVWXYZ";
        for (var i = 0; i < 3; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
    
    var firstName = randomText();
    var secondName = randomText();
    function createCustomerWithFicscalCode() {
        var un = new Date().getTime()
        var cf = new CodiceFiscale({
            name: firstName,
            surname: secondName,
            gender: "M",
            day: 24,
            month: 7,
            year: 1957,
            birthplace: "Francia",
            birthplaceProvincia: "EE"
        });
        console.log("== createCustomerWithFicscalCode " + un + " ==");
        return cf;
    }


    /**
     * Function opens plan list and clicks activateNow
     * possible to defer the end of the process using a promise.
     * @param {Object} exitCode 0 - success, 1 - fail
     */


    beforeEach(function () {

        console.log("========================== Clean cache and Cookies ===================================");

        openDevice(browser);
        browser.waitForText('//div[@class="ds-form__text"][contains(.,"Landline Phone")]');
        browser.deleteCookie()
        browser.sessionStorage('DELETE');
        browser.windowHandleFullscreen();
    })
    
    
    function openDevice(browser) {
        browser.url('http://ilrtvit091:25050/content/digitalexp/vodafone/plan-landline-phone.html');
        browser.waitForText('//div[@class="ds-form__text"][contains(.,"Landline Phone")]');
        browser.pause(5000);


    }

    
    function findOutMoreAboutSelectedPlan(browser, landLinePlanName) {
        console.log("== findOutMoreAboutSelectedPlan " + landLinePlanName + " ==");
        browser.pause(20000)

        closeCookiesFooter()

         console.log("Find out More")
        // browser.debug();
        browser.waitForExist('//div[@class="ds-plans ds-row"]')
        console.log(landLinePlanName);
        browser.waitForVisible('//h3[contains(.,"' + landLinePlanName + '")]');
        browser.scroll('//h3[contains(.,"' + landLinePlanName + '")]');
        // browser.scroll('//h3[contains(.,"All Easy Fixed")]');

        browser.click('//h3[contains(.,"' + landLinePlanName + '")]/../../div[@class="generic-item__btn ds-landline"]//button[*="Discover More"]');
        // browser.click('//h3[contains(.,"All Easy Fixed")]/../../div[@class="generic-item__btn ds-landline"]//button[*="Discover More"]')

        browser.pause(5000)
    }


    function fillServicibilityData(browser) {
        browser.pause(20000)


        closeCookiesFooter()
        browser.waitForExist('//div[@class="ds-servicebility"]');
        browser.scroll('//div[@class="ds-servicebility"]');

        browser.waitForVisible('//h1[contains(.,"Where would you like to install your landline ?")]');
        browser.waitForVisible('//div[@class="ds-address-widget"]');

        browser.scroll("input[name='address.street']");
        console.log("address of the customer")
        browser.setValue("input[name='address.street']", "APPLE")
        browser.setValue("input[name='address.streetNumber']", "3091")

        browser.click('//div[section[input[@name="address.stateOrProvince"]]]')
        browser.waitForVisible('li[value="AL"]')
        browser.click('li[value="AL"]')
        browser.pause(5000)

        console.log("set Address City")
        browser.click('//div[section[input[@name="address.city"]]]')
        browser.waitForVisible('li[value="VILLAMIROGLIO"]')
        browser.click('li[value="VILLAMIROGLIO"]')
        browser.pause(8000)

        console.log("set Address Postal")
        browser.keys("\t51345")
        browser.pause(2000)

        browser.waitForExist('//div[@class="col-xs-12 col-sm-12 col-lg-6 radio-group"]/div[2]/div/div/div')
        browser.click('//div[@class="col-xs-12 col-sm-12 col-lg-6 radio-group"]/div[2]/div/div/div[2]')

        console.log("Continue...")
        browser.waitForVisible('//div[@class="col-xs-6 ds-btn-wrapper"]');
        browser.click('//div[@class="col-xs-6 ds-btn-wrapper"]/button');


        console.log("Servicibility Done .........");

        clickOnStickyFooter();
    }

    function closeCookiesFooter() {
        browser.waitForVisible('#closeicon')
        browser.click('#closeicon')
        browser.pause(10000)
    }


    function shopingCartCheckout(browser) {
        browser.pause(20000)

        // console.log("== findOutMoreAboutSelectedDevice " + planName + " ==");

        closeCookiesFooter()

        console.log("Click on checkout")


        browser.waitForExist('//header[@class="ds-cart-prices-heading"]')
        browser.waitForExist('//footer[@class="ds-micro-cart-totals"]')

        browser.waitForVisible('//footer[@class="ds-micro-cart-totals"]/div/div[3]/button');
        browser.click('//footer[@class="ds-micro-cart-totals"]/div/div[3]/button');


        browser.pause(5000);//div[@class="col-xs-6 ds-btn-wrapper"]/button


    }

    function createCustomer(browser) {
        browser.pause(20000)
        console.log("Navigated to Create Customer");


        browser.waitForVisible('input[name="owningIndividual.firstName"]')
        closeCookiesFooter()

        var cf = createCustomerWithFicscalCode()
        console.log(cf);

        browser.scroll("input[name='owningIndividual.firstName']");
        browser.setValue("input[name='owningIndividual.firstName']", cf.name)
        browser.setValue("input[name='owningIndividual.lastName']", cf.surname)
        browser.setValue("input[name='owningIndividual.fiscalCode']", cf.code)
        browser.keys("\t\t\t\t")

        browser.pause(2000)
        console.log(browser.getText('li[value="2"]'))
        browser.click('li[value="2"]')

        //repeat
        browser.setValue("input[name='owningIndividual.identification.identificationNumber']", new Date().getTime())
        browser.keys("\t")

        browser.waitForExist('li[value="IT"]')
        console.log(browser.getText('li[value="IT"]'))
        browser.click('li[value="IT"]')
        browser.pause(11000)
        // browser.debug();
        // console.log($$('div[class="ds-create-customer ds-container-fluid"] > div > fieldset > div > section > div > div > button'));
        browser.waitForVisible('//section[@class="accordian__body"]/div[6]/div/button')
        browser.click('//section[@class="accordian__body"]/div[6]/div/button')
        browser.pause(15000)


        //set email
        //set the email address like firstname+"@"+secondname+".com"
        console.log("Contact Detail Email")
        browser.setValue("input[name='owningIndividual.email.emailAddress']", firstName + "@" + secondName + ".com").keys("\t")
        browser.setValue("input[name='owningIndividual.email.confirmEmailAddress']", firstName + "@" + secondName + ".com").keys("\t")
        browser.setValue("input[name='owningIndividual.phone.phoneNumber']", "335123456")
        browser.keys("\t")

        //browser.waitForExist('li[value="P"]')
        console.log("after phone number")
        browser.click('li[value="M"]')
        browser.keys("\t")
        browser.pause(8000)

        console.log("oreferred contact time")
        console.log($$('//div[section[input[@name="owningIndividual.preferredContactTime.name"]]]//li'))

        browser.click('li[value="Morning"]')
        browser.keys("\t")
        console.log("address of the customer")
        browser.setValue("input[name='address.street']", "HASOVA")
        browser.setValue("input[name='address.streetNumber']", "3091")
        browser.setValue("input[name='address.mailBox']", "mb")
        //browser.keys("\t")
        browser.pause(8000)


        console.log("set Address State")
        // browser.debug();
        //city and province should be selected from the dropdown. The first one.
        browser.click('//div[section[input[@name="address.stateOrProvince"]]]')
        browser.click('//div[section[input[@name="address.stateOrProvince"]]]')
        browser.click('//div[section[input[@name="address.stateOrProvince"]]]')
        browser.waitForVisible('li[value="AG"]')
        browser.click('li[value="AG"]')
        browser.pause(5000)

        console.log("set Address City")
        browser.click('//div[section[input[@name="address.city"]]]')
        browser.click('//div[section[input[@name="address.city"]]]')
        browser.click('//div[section[input[@name="address.city"]]]')
        browser.waitForVisible('li[value="CALAMONACI"]')
        browser.click('li[value="CALAMONACI"]')
        browser.pause(8000)

        console.log("set Address Postal")
        browser.keys("\t14300")
        browser.pause(2000)

        console.log("Continue...")
        console.log($$('//div[@class="ds-address-widget ds-container-fluid"]//button[@type="submit"]'))
        console.log('clicking on submit button for create customer');
        browser.click('//div[@class="ds-address-widget ds-container-fluid"]//button[@type="submit"]')
        browser.pause(18000)
        // browser.debug();



    }
    function clickOnStickyFooter() {

        browser.pause(15000);
        browser.waitForVisible('//footer[@class="ds-sticky-footer"]/div[2]/div[3]/button[2]');
        browser.click('//footer[@class="ds-sticky-footer"]/div[2]/div[3]/button[2]');
    }


    function deliverySection(browser) {
        browser.pause(20000)
        console.log('====================================Delivery section starts ================================');

        // browser.pause(5000)
        // browser.waitForExist('//div[@class="ds-title col-xs-12"]/span/span')
        // browser.scroll('//div[@class="ds-title col-xs-12"]/span/span');
        // browser.waitForVisible('//li[@class="ds-btn-group__item"]//label[@for="VR_0"]');
        // browser.click('//li[@class="ds-btn-group__item"]//label[@for="VR_0"]')
        // browser.pause(5000)
        // browser.debug();


        // console.log('Clicked on courier option');
        // browser.waitForVisible('//section[@class="ds-radio"]');
        // browser.click('//section[@class="ds-radio"]');

        console.log('Clicked on Provide a copyof your doc to courier');
        browser.pause(5000)
        browser.waitForVisible('//section[@class=" ds-row btn-wrapper"]');
        browser.click('//section[@class=" ds-row btn-wrapper"]/div/button')

    }

 function confirmAndPay(browser) {
     console.log("====================================Confirm and pay section starts=============================");

     browser.pause(25000);
     browser.waitForExist('//span[text()="CONFIRM & PAY"]')


     browser.waitForVisible('//div[@class="ds-form__input--wrapper"]');
     browser.click('//div[@class="ds-form__input--wrapper"]/div')
     browser.pause(5000)
     console.log(' clicked on paper bill ');
     //browser.debug();
     browser.waitForVisible('//ul[@class="ds-consent__list"]/li/div');
     browser.click('//div[@class="ds-form__line mandatory"]/div/div')
     browser.pause(5000)
     console.log(' wireless subscription option 1');
    //  browser.debug();
     browser.waitForVisible('//ul[@class="ds-consent__list"]/li/div');
     browser.click('//div[@class="ds-form__line mandatory error"]/div/div[1]')
     browser.pause(5000)
     console.log(' option 2');

     browser.scroll('//div[@class="ds-tnc"]');
     browser.waitForVisible('//div[@class="ds-tnc"]');
     browser.click('//div[@class="ds-tnc"]/div[2]/ul/li/div/div/div/div');
     browser.pause(10000)
     console.log(' terms and conditons');
     browser.scroll('//div[@class="ds-tnc"]/div/div/span/span');
     browser.waitForVisible('//button[@class="ds-btn ds-btn--large ds-btn--primary"]');
     browser.click('//button[@class="ds-btn ds-btn--large ds-btn--primary"]')
     browser.pause(5000)
     console.log(' final confirm and pay');
     browser.pause(30000);

     browser.click('//div[@class="UXFWidget"]/div/input');
     browser.getUrl();
     browser.pause(25000);
     console.log("We are done with the flow! Thanks for watching!!!")

 }






    //******** test flow
    it('Activate Now Test', function () {
        //open url and load page
        openDevice(browser)

        //planListSelectPlan
        findOutMoreAboutSelectedPlan(browser, "Tutto Facile Fisso");

        fillServicibilityData(browser);

        shopingCartCheckout(browser)

        createCustomer(browser)

        deliverySection(browser)

        confirmAndPay(browser);


    })

    it('Product Config Test', function () {


    })

    afterEach(function () {

    })

})
