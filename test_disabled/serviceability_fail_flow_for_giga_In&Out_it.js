/** UI TEST VFIT */

const expect = require('chai').expect;
CodiceFiscale = require('codice-fiscale-js/dist/codice.fiscale.commonjs2').CodiceFiscale

describe('Flow Test', function () {

  /**
   * Function opens plan list and clicks activateNow
   * possible to defer the end of the process using a promise.
   * @param {Object} exitCode 0 - success, 1 - fail
  */

  function openPlanList(browser) {
    var link = "http://ilrtvit315:25050/content/vodafone/it/self-service-channel/bundle/vodafone-one/list.html";
    browser.url(link);
    console.log(" ============================");
    console.log("You are using env link - " + link);
    console.log(" ============================");
    browser.pause(4000);
    browser.waitForText('//div[@class="ds-container"]/div/div/div[contains(.,"Vodafone One")]');
  }

  function checkServiceability(browser) {
    openPlanList(browser)
    browser.pause(8000)
    console.log("== Serviceability check ==" );
    browser.click('#closeicon')
    browser.waitForExist('//div[@class="UXFWidget"]/div/div/div/div/div/h1/span[text()="Verifica quali offerte sono disponibili nella tua zona"]');
    browser.pause(1000)
    browser.scroll('//div[@class="UXFWidget"]/div/div/div/div/div/h1/span[text()="Verifica quali offerte sono disponibili nella tua zona"]');
    console.log("== Click on check serviceability button =");
    browser.click('//div[@class="UXFWidget"]/div/div/div/div/div/button');
    browser.pause(5000)
    

    browser.setValue('//div[@class="ds-container-fluid "]/div/div/div/div/div/input[@name="address.street"]',"via affamata")
    browser.pause(2000)
    browser.setValue('//div[@class="ds-container-fluid "]/div/div/div/div/div/input[@name="address.streetNumber"]',"12312")
    browser.pause(2000)
    console.log("select province and city");
    browser.doubleClick('//div[section[input[@name="address.stateOrProvince"]]]')
    browser.pause(6000)
    browser.doubleClick('//div[section[input[@name="address.stateOrProvince"]]]')
    browser.click('//div[section[input[@name="address.stateOrProvince"]]]')
    
    browser.waitForVisible('li[value="AG"]')
    browser.click('li[value="AG"]')
    browser.doubleClick('//div[section[input[@name="address.city"]]]')
    browser.pause(6000)
    browser.doubleClick('//div[section[input[@name="address.city"]]]')
    browser.click('//div[section[input[@name="address.city"]]]')
    
    browser.waitForVisible('li[value="AGRIGENTO"]')
    browser.click('li[value="AGRIGENTO"]')
    console.log("Enter postal code");
    //browser.setValue('//input[@name="address.postalCode"]',"15009")  //Error coming need to check this line
    browser.keys("\t15015")
    browser.click('//div[@class="col-xs-12 col-sm-12 col-lg-6 radio-group"]/div/div/div/div/label[@for="do_not_transfer_1"]')
    browser.pause(3000)
    browser.click('//button[@type="submit"]/span[text()="Verifica disponibilità"]')
  }

  function planListSelectPlanDiscoverMore(browser, planName) {
    console.log("== planListSelectPlanAcivateNow " + planName + " ==" );
    openPlanList(browser)
    browser.waitForVisible('#closeicon')
    browser.click('#closeicon')
    browser.pause(15000)
    
    console.log("Vodafone one plan list Page")
    console.log(browser.getText('//h3'))
    
    console.log("Click Discover More")
    

    browser.scroll('//h3[@title="'+planName+'"]')
    //browser.scroll('//h3[@title="Vodafone One Internet"]')
    //browser.click('//li[contains(.,"Vodafone One Internet")]//button[*="Discover More"]')
    browser.click('//li[contains(.,"' + planName + '")]//button[*="Discover More"]')
    browser.pause(5000)
  }

  function selectPlanAfterServiceabilityCheck(browser, planName){
    console.log("=======================================");
    console.log("== select buy now of serviceable offer - " + planName + " ==" );
    console.log("=======================================");
    browser.pause(10000)
    
    console.log("Vodafone one plan list Page")
    console.log(browser.getText('//h3'))
    
    console.log("Click Buy Now")

    //browser.scroll('//h3[@title="'+planName+'"]')
    browser.pause(5000)
    browser.click('//div[contains(.,"' + planName + '")]//button[*="Scopri di più"]')
    //browser.click('//div[contains(.,"Vodafone One Internet")]//button[*="Buy Now"]')
    console.log("=======================================");
    console.log("moving to product config")
    browser.pause(15000)
    console.log(planName + " selected and moving to product config");
    console.log("=======================================");
  }

  function selectDevice(browser, deviceName){
    console.log("== selecting the device - " + deviceName + " ==" );
    browser.click('#closeicon')
    browser.pause(3000);
    browser.scroll('div[class="ds-panel__header"]')
    
    browser.pause(10000);
    console.log("=======================================");
    console.log("verifying number of devices");
    console.log(browser.getText('//div[@class="ds-select-device-item "]'));
    console.log("=======================================");
    expect(browser.getText('//div[@class="ds-select-device-item "]').length) >= (1);

    console.log("selecting device.")
    browser.pause(2000);
    browser.click('//div[@class="ds-select-device-item__wrapper"]/div[contains(.,"' + deviceName + '")]//button[*="Select"]')
    browser.pause(3000)
    browser.waitForVisible('//h2[contains(.,"' + deviceName + '")]')
    //browser.waitForVisible('//h2[contains(.,"iPhone 8 Plus")]')
    browser.click('//button[@class="ds-btn ds-btn--primary"]/span/span')
    //browser.click('//button[@class="ds-btn ds-btn--primary"]/span/span')
    browser.pause(15000);
  }

  function noDevice(browser, buttonName){
    console.log("select -->  I don't need Device");
    browser.scroll('div[class="ds-panel__header"]')
    browser.click('//div[@class="ds-panel__header"]/button[contains(.,"' + buttonName +'")]')
    browser.pause(8000);
  }


  function randomText() {
    var text = "";
    var possible = "BCDFGHJKLMNPQRSTVWXYZ";
    for (var i = 0; i < 3; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  var firstName = randomText();
  var secondName = randomText();
  function createCustomerWithFicscalCode() {
    var un = new Date().getTime()
    var cf = new CodiceFiscale({
      name: firstName,
      surname: secondName,
      gender: "M",
      day: 24,
      month: 7,
      year: 1957,
      birthplace: "Francia",
      birthplaceProvincia: "EE"
    });
    console.log("=======================================");
    console.log("== createCustomerWithFicscalCode " + un + " ==");
    console.log("=======================================");
    return cf;
  }


  beforeEach(function () {
    var caps = browser.session();
    console.log(browser.session('get').sessionId); // throws an error
    console.log("== ======================= ==");
    console.log("== Clean cache and Cookies ==");
    openPlanList(browser);
    browser.deleteCookie()
    browser.sessionStorage('DELETE');
    browser.windowHandleSize({ width: 1300, height: 1000 });

  })
  

  //******** test flow
  it('Vodafone one test with serviceability fail', function () {
    //checking the serviceability
    checkServiceability(browser);
    selectPlanAfterServiceabilityCheck(browser, "Giga In&Out")
    console.log("=======================================");
    console.log("----- Clicking CONTINUE button on the device details screen -----");
    console.log("=======================================");
    
    noDevice(browser, "Ho giá il mio smartphone")  //change the button name of "I don't need device" if language changes.
    
    console.log("=======================================");
    console.log("Back to the product configuration page.")
    console.log("======================================="); 

    console.log("Click continue on product configuraiton page.");
    browser.click('//div[@class="ds-sticky-footer__action-btn"]/button[contains(.,"Continua")]')

    console.log("Moving to shopping cart page");
    //browser.waitForVisible('#closeicon', 5000)
    //browser.click('#closeicon')
    console.log("=======================================");
    console.log("Click continue on shopping cart page");
    console.log("=======================================");
    browser.pause(10000)
    browser.click('div[class=ds-sticky-footer__action-btn] > button')


    //+++++++++++++++++//
    console.log("Checkout Page")
   // browser.waitForVisible('div[class=ds-sticky-footer__action-btn] > button')

    browser.pause(15000)
    // browser.waitForVisible('#closeicon')
    // browser.click('#closeicon')
    //browser.click('div[class=ds-sticky-footer__action-btn] > button')

    console.log("Contact Fiscal Code")
    browser.waitForVisible('input[name="owningIndividual.firstName"]')
    //browser.waitForVisible('#closeicon')
    //browser.click('#closeicon')
    var cf = createCustomerWithFicscalCode()
    console.log(cf);

    browser.scroll("input[name='owningIndividual.firstName']");
    browser.setValue("input[name='owningIndividual.firstName']", cf.name)
    browser.setValue("input[name='owningIndividual.lastName']", cf.surname)
    browser.setValue("input[name='owningIndividual.fiscalCode']", cf.code)
    browser.keys("\t\t\t\t")

    browser.pause(2000)
    console.log(browser.getText('li[value="2"]'))
    browser.click('li[value="2"]')

    
    browser.setValue("input[name='owningIndividual.identification.identificationNumber']", new Date().getTime())
    browser.keys("\t")

    browser.waitForExist('li[value="IT"]')
    console.log(browser.getText('li[value="IT"]'))
    browser.click('li[value="IT"]')

    //the pause below if decreased then causing hte failure of the below button click.
    browser.pause(15000)
    
    console.log($$('div[class="ds-create-customer ds-container-fluid"] > div > div > div > section > div > div > button'));
    browser.click('div[class="ds-create-customer ds-container-fluid"] > div > div > div > section > div > div > button')
    browser.pause(5000)
    console.log("=======================================");
    console.log("Contact Detail Email")
    console.log("=======================================");
    browser.setValue("input[name='owningIndividual.email.emailAddress']", firstName + "@" + secondName + ".com").keys("\t")
    browser.setValue("input[name='owningIndividual.email.confirmEmailAddress']", firstName + "@" + secondName + ".com").keys("\t")
    browser.setValue("input[name='owningIndividual.phone.phoneNumber']", "335123456")
    browser.keys("\t")

    

    //browser.waitForExist('li[value="P"]')
    console.log("=======================================");
    console.log("after phone number")
    console.log("=======================================");
    browser.click('li[value="M"]')
    browser.keys("\t")
    browser.pause(8000)
    console.log("Preferred contact time")
    console.log($$('//div[section[input[@name="owningIndividual.preferredContactTime.name"]]]//li'))

    browser.click('li[value="Morning"]')
    browser.keys("\t")
    console.log("=======================================");
    console.log("address of the customer")
    console.log("=======================================");
    browser.setValue("input[name='address.street']", "HASOVA")
    browser.setValue("input[name='address.streetNumber']", "3091")
    browser.setValue("input[name='address.mailBox']", "mb")
    //browser.keys("\t")
    browser.pause(8000)

    browser.scroll('//div[@class="ds-row"]/div/div/span[text()="Address"]');
    console.log("set Address State")
    browser.doubleClick('//div[section[input[@name="address.stateOrProvince"]]]')
    browser.pause(6000)
    browser.doubleClick('//div[section[input[@name="address.stateOrProvince"]]]')
    browser.click('//div[section[input[@name="address.stateOrProvince"]]]')
    browser.waitForVisible('li[value="AL"]')
    browser.click('li[value="AL"]')
    

    console.log("set Address City")
    browser.doubleClick('//div[section[input[@name="address.city"]]]')
    browser.pause(6000)
    browser.doubleClick('//div[section[input[@name="address.city"]]]')
    browser.click('//div[section[input[@name="address.city"]]]')
    browser.waitForVisible('li[value="VILLAMIROGLIO"]')
    browser.click('li[value="VILLAMIROGLIO"]')
    
    console.log("set Address Postal")
    browser.keys("\t15009")
    browser.pause(2000)

    console.log("Continue...")

    

    console.log($$('//div[@class="ds-address-widget ds-container-fluid"]//button[@type="submit"]'))
    console.log('clicking on submit button for create customer');
    browser.click('//div[@class="ds-address-widget ds-container-fluid"]//button[@type="submit"]')
    browser.pause(20000)
    //console.log("Navigating to phone ...")

    //browser.waitForVisible('//div[@class="col-xs-12 ds-phone-number__frame"]')
    //browser.pause(15000)
    //console.log('clicking on phone option');
    //browser.scroll('//div[@class="ds-phone-number container"]');
    //browser.waitForVisible('//li[@class="ds-btn-group__item"]/label[@for="activateNewNumber_1"]');

    //browser.click('//li[@class="ds-btn-group__item"]/label[@for="activateNewNumber_1"]')
    //browser.pause(5000)
    //browser.waitForVisible('//div[@class="btn-wrapper col-xs-12"]//button');
    //console.log('click on phone continue');
    //browser.click('//div[@class="btn-wrapper col-xs-12"]//button')

    //browser.pause(8000)

    console.log('Delivery section starts');
    browser.waitForExist('//div[@class="ds-title col-xs-12"]/span/span')
    browser.scroll('//div[@class="ds-title col-xs-12"]/span/span');
    browser.waitForVisible('//li[@class="ds-btn-group__item"]//label[@for="CO_0"]');
    browser.click('//li[@class="ds-btn-group__item"]//label[@for="CO_0"]')
    browser.pause(5000)
    
    console.log('Clicked on Provide a copyof your doc to courier');
    browser.waitForVisible('//section[@class=" ds-row btn-wrapper"]');
    browser.click('//section[@class=" ds-row btn-wrapper"]/div/button')
    console.log("=======================================");
    console.log("=====Confirm and pay section starts--=======");
    console.log("=======================================");
    browser.waitForExist('//span[text()="Conferma e Paga"]')
    browser.pause(5000)
    browser.scroll('//div[@class="ds-confirm-and-pay core-radio"]');
    browser.waitForVisible('//div[@class="ds-form__input--wrapper"]');
    browser.click('//div[@class="ds-form__input--wrapper"]/div')
    browser.scroll('//span[text()="Conferma e Paga"]')
    browser.pause(5000)
    console.log(' clicked on paper bill ');
    browser.waitForVisible('//ul[@class="ds-consent__list"]/li/div');
    browser.click('//div[@class="ds-form__line mandatory"]/div/div')
    browser.pause(1000)
    

    console.log(' option 2');
    browser.scroll('//ul[@class="ds-consent__list"]/li/div')
 
    browser.waitForVisible('//ul[@class="ds-consent__list"]/li/div');
    browser.scroll('//div[@class="ds-tnc"]');
    browser.waitForVisible('//div[@class="ds-tnc"]');
    browser.click('//div[@class="ds-tnc"]/div[2]/ul/li/div/div/div/div')
    browser.pause(5000)
    console.log(' terms and conditons');
    browser.scroll('//div[@class="ds-tnc"]/div/div/span/span');
    browser.waitForVisible('//button[@class="ds-btn ds-btn--large ds-btn--primary"]');
    browser.click('//button[@class="ds-btn ds-btn--large ds-btn--primary"]')
    browser.pause(5000)
    console.log(' final confirm and pay');
    browser.pause(20000);
  
    browser.click('//div[@class="UXFWidget"]/div/input')
    browser.pause(10000)

  })

 

  afterEach(function () {
  })

})
