/** UI TEST VFIT */

const expect = require('chai').expect;
CodiceFiscale = require('codice-fiscale-js/dist/codice.fiscale.commonjs2').CodiceFiscale

describe('Flow Test', function () {

    /**
     * Function opens plan list and clicks activateNow
     * possible to defer the end of the process using a promise.
     * @param {Object} exitCode 0 - success, 1 - fail
     */


    beforeEach(function () {
        var caps = browser.session();
        //console.log(browser.desiredCapabilities);
        console.log(browser.session('get').sessionId); // throws an error
        console.log("== ======================= ==");
        console.log("== Clean cache and Cookies ==");

        openPlanList(browser);

        browser.waitForText('//div[@class="ds-tablet-plans__container row"]');
        browser.deleteCookie()
        browser.sessionStorage('DELETE');
        browser.windowHandleFullscreen();
    })


    function randomText() {
        var text = "";
        //Here vowels are taken into possible just to make the generation of fiscal code easier.
        var possible = "BCDFGHJKLMNPQRSTVWXYZ";
        for (var i = 0; i < 3; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
    var firstName = randomText();
    var secondName = randomText();
    function createCustomerWithFicscalCode() {
        var un = new Date().getTime()
        var cf = new CodiceFiscale({
            name: firstName,
            surname: secondName,
            gender: "M",
            day: 24,
            month: 7,
            year: 1957,
            birthplace: "Francia",
            birthplaceProvincia: "EE"
        });
        console.log("== createCustomerWithFicscalCode " + un + " ==");
        return cf;
    }


    function openPlanList(browser) {
        browser.url('http://ilrtvit091:25050/content/digitalexp/vodafone/tablet-plan-list.html');
        // browser.debug();
        browser.waitForText('//div[@class="ds-tablet-plans__container row"]');
        browser.pause(5000)
    }

    function planListSelectPlanDiscoverMore(browser, planName) {
        console.log("== planListSelectPlanAcivateNow " + planName + " ==" );

        browser.waitForVisible('#closeicon')
        browser.click('#closeicon')
        browser.pause(15000)

        console.log("Click Discover More")
        browser.waitForExist('div[class="ds-plan-action__wrapper"] > button')
        browser.scroll('//div[@title="'+planName+'"]')
        browser.click('//li[contains(.,"' + planName + '")]//button[*="Discover More"]')
        browser.pause(5000)

    }

    function selectDevice(browser, deviceName){
        console.log("== planListSelectPlanAcivateNow " + deviceName + " ==" );
        console.log("Product configuration page.")

        browser.pause(5000);
        browser.waitForText('div[class="ds-panel__header"]')
        browser.scroll('div[class="ds-panel__header"]')
        browser.click('#closeicon')
        browser.pause(20000);

        console.log("verifying number of devices");

        console.log(browser.getText('//div[@class="ds-select-device-item "]'));

        expect(browser.getText('//div[@class="ds-select-device-item "]').length) >= (1);

        console.log("selecting device.");
        browser.waitForVisible('#closeicon')

        browser.scroll('div[class="ds-panel__header"]')
        browser.pause(10000);

        // browser.debug();
        browser.waitForVisible('//div[@class="ds-select-device-item__wrapper"]/div[contains(.,"' + deviceName + '")]//button[*="Select"]')
        browser.click('//div[@class="ds-select-device-item__wrapper"]/div[contains(.,"' + deviceName + '")]//button[*="Select"]')
        // browser.waitForVisible('//div[@class="ds-select-device-item__wrapper"]/div[contains(.,"EASY ZTE")]//button[*="Select"]')
        // browser.click('//div[@class="ds-select-device-item__wrapper"]/div[contains(.,"' + deviceName + '")]//button[*="Select"]')
        browser.pause(3000)

        // browser.debug();
        console.log("Continue.");
        browser.waitForVisible('//div[@class="ds-device-info__description-footer"]/button')
        browser.scroll('//div[@class="ds-device-info__description-footer"]/button')
        browser.click('//div[@class="ds-device-info__description-footer"]/button')

        browser.pause(25000);

        console.log("Continue. to DEvice");
        // browser.waitForVisible('#closeicon')
        // browser.click('#closeicon')
        browser.pause(15000);
        console.log("close button clicked");
        // browser.debug();



        browser.waitForVisible('//footer[@class="ds-sticky-footer"]/div[2]/div[3]/button[2]');
        browser.click('//footer[@class="ds-sticky-footer"]/div[2]/div[3]/button[2]');
    }


    //******** test flow***********************//
    it('Activate Now Test', function () {

        openPlanList(browser)

        planListSelectPlanDiscoverMore(browser, "Total Giga");

        selectDevice(browser, "FACILE HUAWEI")

        shopingCartCheckout(browser)

        createCustomerTillConfirmPage(browser)

    })

    function shopingCartCheckout(browser) {

        // console.log("== findOutMoreAboutSelectedDevice " + planName + " ==");

        browser.waitForVisible('#closeicon')
        browser.click('#closeicon')
        browser.pause(5000)

        console.log("Click on checkout")


        browser.waitForExist('//header[@class="ds-cart-prices-heading"]')
        browser.waitForExist('//footer[@class="ds-micro-cart-totals"]')

        browser.waitForVisible('//footer[@class="ds-micro-cart-totals"]/div/div[3]/button');
        browser.click('//footer[@class="ds-micro-cart-totals"]/div/div[3]/button');


        browser.pause(5000);


    }

    function createCustomerTillConfirmPage(browser) {
        console.log("Contact Fiscal Code");
        //   console.log("Contact Fiscal Code");
        //   // browser.debug();
        browser.waitForVisible('input[name="owningIndividual.firstName"]')
        browser.waitForVisible('#closeicon')
        browser.click('#closeicon')

        var cf = createCustomerWithFicscalCode()
        console.log(cf);

        browser.scroll("input[name='owningIndividual.firstName']");
        browser.setValue("input[name='owningIndividual.firstName']", cf.name)
        browser.setValue("input[name='owningIndividual.lastName']", cf.surname)
        browser.setValue("input[name='owningIndividual.fiscalCode']", cf.code)
        browser.keys("\t\t\t\t")
        //browser.click('//div[section[input[@name="owningIndividual.identification.identificationType"]]]')
        //browser.click('//div[section[input[@name="owningIndividual.identification.identificationType"]]]')

        browser.pause(2000)
        console.log(browser.getText('li[value="2"]'))
        browser.click('li[value="2"]')

        //repeat
        browser.setValue("input[name='owningIndividual.identification.identificationNumber']", new Date().getTime())
        browser.keys("\t")
        //browser.click('//div[section[input[@name="owningIndividual.identification.nationality"]]]')

        browser.waitForExist('li[value="IT"]')
        console.log(browser.getText('li[value="IT"]'))
        browser.click('li[value="IT"]')
        browser.pause(11000)

        console.log($$('div[class="ds-create-customer ds-container-fluid"] > div > fieldset > div > section > div > div > button'));
        browser.click('div[class="ds-create-customer ds-container-fluid"] > div > fieldset > div > section > div > div > button')
        browser.pause(15000)

        //set email
        //set the email address like firstname+"@"+secondname+".com"
        console.log("Contact Detail Email")
        browser.setValue("input[name='owningIndividual.email.emailAddress']", firstName + "@" + secondName + ".com").keys("\t")
        browser.setValue("input[name='owningIndividual.email.confirmEmailAddress']", firstName + "@" + secondName + ".com").keys("\t")
        browser.setValue("input[name='owningIndividual.phone.phoneNumber']", "335123456")
        browser.keys("\t")
        //input[name='owningIndividual.preferredContactMethod.name
        //console.log($$('//div[section[input[@name="owningIndividual.preferredContactMethod.name"]]]'))
        //browser.click('//div[section[input[@name="owningIndividual.preferredContactMethod.name"]]]')
        //browser.click('//div[section[input[@name="owningIndividual.preferredContactMethod.name"]]]')

        //browser.waitForExist('li[value="P"]')
        console.log("after phone number")
        browser.click('li[value="M"]')
        browser.keys("\t")
        browser.pause(8000)

        //browser.click('//div[section[input[@name="owningIndividual.preferredContactTime.name"]]]')
        //browser.click('//div[section[input[@name="owningIndividual.preferredContactTime.name"]]]')
        console.log("oreferred contact time")
        console.log($$('//div[section[input[@name="owningIndividual.preferredContactTime.name"]]]//li'))

        browser.click('li[value="Morning"]')
        browser.keys("\t")
        console.log("address of the customer")
        browser.setValue("input[name='address.street']", "HASOVA")
        browser.setValue("input[name='address.streetNumber']", "3091")
        browser.setValue("input[name='address.mailBox']", "mb")
        //browser.keys("\t")
        browser.pause(8000)


        console.log("set Address State")
        //browser.click('//div[section[input[@name="address.stateOrProvince"]]]')
        // //browser.click('//div[section[input[@name="address.stateOrProvince"]]]')

        // browser.waitForVisible('li[value="AO"]')
        // browser.click('li[value="AO"]')
        // browser.keys("\t")
        // browser.pause(15000)

        // browser.click('//div[section[input[@name="address.stateOrProvince"]]]')
        // browser.waitForVisible('li[value="AN"]')
        // browser.click('li[value="AN"]')
        // browser.pause(5000)

        //city and province should be selected from the dropdown. The first one.
        browser.click('//div[section[input[@name="address.stateOrProvince"]]]')
        browser.waitForVisible('li[value="AL"]')
        browser.click('li[value="AL"]')
        browser.pause(5000)

        console.log("set Address City")
        browser.click('//div[section[input[@name="address.city"]]]')  ;
        browser.waitForVisible('li[value="VILLAMIROGLIO"]')
        browser.click('li[value="VILLAMIROGLIO"]')
        browser.pause(8000)

        //pick one of the valid postal codes from : 15010 to 15079
        function getRandomArbitrary(min, max) {
            return Math.random() * (max - min) + min;
        }
        console.log("set Address Postal")
        // browser.debug();
        browser.keys("\t14300")

        // browser.setValue("input[name='address.postalCode']", "15010");
        browser.pause(2000)

        console.log("Continue...")
        console.log($$('//div[@class="ds-address-widget ds-container-fluid"]//button[@type="submit"]'))
        console.log('clicking on submit button for create customer');
        browser.click('//div[@class="ds-address-widget ds-container-fluid"]//button[@type="submit"]')
        browser.pause(18000)
        //  browser.click('//div[@class="ds-address-widget ds-container-fluid"]//button[@type="submit"]')
        browser.pause(20000)
        console.log("Navigating to phone ...")
        // browser.debug();
        // browser.waitForVisible('//div[@class="col-xs-12 ds-phone-number__frame"]')
        browser.pause(15000)

        console.log('Delivery section starts');
        // browser.debug();
        browser.waitForExist('//div[@class="ds-title col-xs-12"]/span/span')
        browser.scroll('//div[@class="ds-title col-xs-12"]/span/span');
        browser.pause(5000)

        console.log('Clicked on Provide a copyof your doc to courier');
        browser.waitForVisible('//section[@class=" ds-row btn-wrapper"]');
        browser.click('//section[@class=" ds-row btn-wrapper"]/div/button')

        console.log("=====Confirm and pay section starts--=======");
        // browser.debug();
        browser.pause(25000)
        browser.waitForExist('//span[text()="CONFIRM & PAY"]')
        browser.pause(5000)

        browser.waitForVisible('//div[@class="ds-form__input--wrapper"]');
        browser.click('//div[@class="ds-form__input--wrapper"]/div')
        browser.pause(5000)
        console.log(' clicked on paper bill ');
        //browser.debug();
        browser.waitForVisible('//ul[@class="ds-consent__list"]/li/div');
        browser.click('//div[@class="ds-form__line mandatory"]/div/div')
        browser.pause(5000)
        console.log(' wireless subscription option 1');

        browser.scroll('//div[@class="ds-tnc"]');
        browser.waitForVisible('//div[@class="ds-tnc"]');
        browser.click('//div[@class="ds-tnc"]/div[2]/ul/li/div/div/div/div')
        browser.pause(5000)
        console.log(' terms and conditons');
        browser.scroll('//div[@class="ds-tnc"]/div/div/span/span');
        browser.waitForVisible('//button[@class="ds-btn ds-btn--large ds-btn--primary"]');
        browser.click('//button[@class="ds-btn ds-btn--large ds-btn--primary"]')
        browser.pause(5000)
        console.log(' final confirm and pay');
        browser.pause(30000);
        //   ///html/body/section/cq:include/cq:includeclientlib/cq:includeclientlib/cq:includeclientlib/div[1]/div/div/div[2]/div/input
        //   browser.debug();
        browser.click('//div[@class="UXFWidget"]/div/input');
        browser.getUrl();
        //   /*
        // .waitForElementVisible('div[class="ds-address-widget ds-container-fluid"] > div > fieldset > div > div > div > div > button', 20000)

        //   */
        browser.pause(25000)
        console.log('full flow completed');

    }

    it('Product Config Test', function () {


    })

    afterEach(function () {
        //browser.debug()
    })

})
