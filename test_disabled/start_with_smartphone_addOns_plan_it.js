/** UI TEST VFIT */

const expect = require('chai').expect;
CodiceFiscale = require('codice-fiscale-js/dist/codice.fiscale.commonjs2').CodiceFiscale

describe('Flow Test', function () {

    /**
     * Function opens plan list and clicks activateNow
     * possible to defer the end of the process using a promise.
     * @param {Object} exitCode 0 - success, 1 - fail
     */


    beforeEach(function () {

        console.log("========================== Clean cache and Cookies ===================================");

        browser.url("https://www.google.com/")
        //browser.waitForText('//div[@class="device-list-title"]/div/div/div/span[contains(.,"Smartphones")]');
        browser.deleteCookie()
        browser.sessionStorage('DELETE');
        browser.windowHandleSize({ width: 1300, height: 1000 });
    })

    function openPlanList(browser) {
        var link = "http://ilrtvit315:25050/content/vodafone/it/self-service-channel/mobile/device/list.html";
        browser.url(link);
        console.log(" ============================");
        console.log("You are using env link - " + link);
        console.log(" ============================");
        browser.waitForText('//div[@class="device-list-title"]/div/div/div/span[contains(.,"Smartphones")]');
        browser.pause(10000);
        
        browser.pause(15000)
      }


    function randomText() {
        var text = "";
        //Here vowels are taken into possible just to make the generation of fiscal code easier.
        var possible = "BCDFGHJKLMNPQRSTVWXYZ";
        for (var i = 0; i < 3; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
    var firstName = randomText();
    var secondName = randomText();
    function createCustomerWithFicscalCode() {
        var un = new Date().getTime()
        var cf = new CodiceFiscale({
            name: firstName,
            surname: secondName,
            gender: "M",
            day: 24,
            month: 7,
            year: 1993,
            birthplace: "Francia",
            birthplaceProvincia: "EE"
        });
        console.log("== createCustomerWithFicscalCode " + un + " ==");
        return cf;
    }

    function selectDevice(browser, deviceName){
        console.log("== selecting the device - " + deviceName + " ==" );
       
        browser.waitForExist('#closeicon',10000)
        browser.click('#closeicon')
        
        browser.scroll('//div[@class="device-list-title"]/div/div/div/span[contains(.,"Smartphones")]');
        
        browser.pause(10000);
        console.log("=======================================");
        console.log("verifying number of devices");
        console.log(browser.getText('//div[@class="ds-select-device-item "]'));
        console.log("=======================================");
        expect(browser.getText('//div[@class="ds-select-device-item "]').length) >= (1);
    
        console.log("selecting device.")
        browser.pause(2000);
        browser.click('//div[@class="ds-select-device-item__wrapper"]/div[contains(.,"' + deviceName + '")]//button[*="Scopri di più"]')//Scopri di più / Select
        
        // browser.pause(3000)
        // browser.waitForVisible('//h2[contains(.,"' + deviceName + '")]')
        // //browser.waitForVisible('//h2[contains(.,"iPhone 8 Plus")]')
        // browser.click('//button[@class="ds-btn ds-btn--primary"]/span/span')
        // //browser.click('//button[@class="ds-btn ds-btn--primary"]/span/span')
        browser.pause(15000);
      }

    function planListSelectPlanAcivateNow(browser, planName) {
        console.log("== planListSelectPlanAcivateNow " + planName + " ==");
        //openPlanList(browser)
        browser.waitForVisible('#closeicon')
        browser.click('#closeicon')
        browser.pause(12000)
        //below code is applicable when starting with device
        //<----------------------------------------------->

        browser.scroll('//div[@class="personalize-your-offer"]')
        browser.scroll('//h3[text()="'+ planName +'"]');


        //<----------------------------------------------->
        //check 3 plans Pro
        console.log("Plan list Page")

         //click ActivateNow
        console.log("Click Activate Now")

        //browser.waitForExist('div[class="ds-plan-action__wrapper"] > button')
        console.log(planName);

        //browser.scroll('//div[@title="'+ planName +'"]');
        browser.click('//li[contains(.,"' + planName + '")]//button[*="Seleziona"]')
        browser.pause(10000)
    }

    function phoneNumber(){
        browser.waitForVisible('//div[@class="col-xs-12 ds-phone-number__frame"]')
        browser.pause(15000)
        console.log('clicking on phone option');
        browser.scroll('//div[@class="ds-phone-number container"]');
        browser.waitForVisible('//li[@class="ds-btn-group__item"]/label[@for="activateNewNumber_1"]');
    
        browser.click('//li[@class="ds-btn-group__item"]/label[@for="activateNewNumber_1"]')
        browser.pause(5000)
        browser.waitForVisible('//div[@class="btn-wrapper col-xs-12"]//button');
        console.log('click on phone continue');
        browser.click('//div[@class="btn-wrapper col-xs-12"]//button')
      }

    //******** test flow
    it('Select iPhone 8 then Vodafone RED', function () {
        openPlanList(browser)
        selectDevice(browser,"iPhone 8 Plus")
        planListSelectPlanAcivateNow(browser, "Vodafone RED")

        browser.pause(8000)
        browser.waitForVisible('#closeicon')
        browser.click('#closeicon')
        browser.scroll('//div[@class="personalize-your-offer"]')
        browser.scroll('//div[@class="ds-addons--container"]')

        console.log("verify number of add on is one or more")
        expect(browser.getText('//div[@class="slick-track"]').length) >= (1);
        console.log("adding vodafone music and video pass");
        browser.pause(2000)
        browser.click('//div[contains(.,"Vodafone Pass Music")]//button[*="Aggiungi"]')
        browser.pause(15000)
        browser.click('//div[contains(.,"Vodafone Pass Video")]//button[*="Aggiungi"]')
        browser.pause(15000)

        console.log("Click continue on product configuraiton page.");
        browser.click('//div[@class="ds-sticky-footer__action-btn"]/button[contains(.,"Continua")]')
        browser.pause(10000)
        console.log("shopping cart page")
        browser.waitForVisible('#closeicon', 20000)
        browser.click('#closeicon')
        browser.waitForVisible('div[class=ds-sticky-footer__action-btn] > button')
        browser.click('div[class=ds-sticky-footer__action-btn] > button')

        browser.pause(10000)
        console.log("Checkout process page")
        browser.waitForVisible('#closeicon',20000)
        browser.click('#closeicon')
        console.log("Contact Fiscal Code");
        browser.waitForVisible('input[name="owningIndividual.firstName"]')
        var cf = createCustomerWithFicscalCode()
        console.log(cf);

        browser.scroll("input[name='owningIndividual.firstName']");
        browser.setValue("input[name='owningIndividual.firstName']", cf.name)
        browser.setValue("input[name='owningIndividual.lastName']", cf.surname)
        browser.setValue("input[name='owningIndividual.fiscalCode']", cf.code)
        browser.keys("\t\t\t\t")

        browser.pause(2000)
        console.log(browser.getText('li[value="2"]'))
        browser.click('li[value="1"]')
        browser.pause(6000)
        //repeat
        browser.setValue("input[name='owningIndividual.identification.identificationNumber']", new Date().getTime())
        browser.keys("\t")
        browser.pause(5000)
        browser.waitForExist('li[value="IT"]')
        console.log(browser.getText('li[value="IT"]'))
        browser.click('li[value="IT"]')
        browser.pause(10000)
        

        if($$('div[class="ds-create-customer ds-container-fluid"] > div > div > div > section > div > div > button')!=''){
                browser.click('div[class="ds-create-customer ds-container-fluid"] > div > div > div > section > div > div > button')
            }
            else{
                browser.pause(15000)
                browser.click('div[class="ds-create-customer ds-container-fluid"] > div > div > div > section > div > div > button')
            }
        
        console.log();
        
        browser.pause(15000)

        console.log("Contact Detail Email")
        browser.setValue("input[name='owningIndividual.email.emailAddress']", firstName + "@" + secondName + ".com").keys("\t")
        browser.setValue("input[name='owningIndividual.email.confirmEmailAddress']", firstName + "@" + secondName + ".com").keys("\t")
        browser.setValue("input[name='owningIndividual.phone.phoneNumber']", "335123456")
        browser.keys("\t")

        console.log("after phone number")
        browser.click('li[value="M"]')
        browser.keys("\t")
        browser.pause(8000)

        console.log("oreferred contact time")
        console.log($$('//div[section[input[@name="owningIndividual.preferredContactTime.name"]]]//li'))

        browser.click('li[value="Morning"]')
        browser.keys("\t")
        console.log("address of the customer")
        browser.setValue("input[name='address.street']", "HASOVA")
        browser.setValue("input[name='address.streetNumber']", "3091")
        browser.setValue("input[name='address.mailBox']", "mb")
        browser.pause(8000)


        console.log("set Address State")

        //city and province should be selected from the dropdown. The first one.
        browser.click('//div[section[input[@name="address.stateOrProvince"]]]')
        browser.waitForVisible('li[value="AL"]')
        browser.click('li[value="AL"]')
        browser.pause(5000)

        console.log("set Address City")
        browser.click('//div[section[input[@name="address.city"]]]')
        browser.waitForVisible('li[value="VILLAMIROGLIO"]')
        browser.click('li[value="VILLAMIROGLIO"]')
        browser.pause(8000)

        console.log("set Address Postal")
        browser.keys("\t14300")
        browser.pause(2000)

        console.log("Continue...")
        console.log($$('//div[@class="ds-address-widget ds-container-fluid"]//button[@type="submit"]'))
        console.log('clicking on submit button for create customer');
        browser.click('//div[@class="ds-address-widget ds-container-fluid"]//button[@type="submit"]')
        browser.pause(20000)
        console.log('====================================Phone section starts ================================');

            phoneNumber()

        console.log('====================================Delivery section starts ================================');

        browser.pause(5000)
        browser.waitForExist('//div[@class="ds-title col-xs-12"]/span/span')
        browser.scroll('//div[@class="ds-title col-xs-12"]/span/span');
        browser.pause(5000)
        browser.waitForVisible('//section[@class=" ds-row btn-wrapper"]');
        browser.click('//section[@class=" ds-row btn-wrapper"]/div/button')

        console.log("====================================Confirm and pay section starts=============================");

        browser.pause(18000);
        browser.waitForExist('//span[text()="Conferma e Paga"]')
        browser.pause(5000)
        browser.scroll('//div[@class="ds-confirm-and-pay core-radio"]');
        browser.waitForVisible('//div[@class="ds-form__input--wrapper"]');
        browser.click('//div[@class="ds-form__input--wrapper"]/div')
        browser.scroll('//span[text()="Conferma e Paga"]')

        browser.pause(2000)
        console.log(' clicked on paper bill ');
        browser.waitForVisible('//ul[@class="ds-consent__list"]/li/div');
        browser.click('//div[@class="ds-form__line mandatory"]/div/div')
        browser.pause(1000)
        console.log(' wireless subscription option 1');
        browser.waitForVisible('//ul[@class="ds-consent__list"]/li/div');
        browser.click('//li[@class="ds-consent__item"]/div/div/div/div/label[@for="Mobile--19680--Yes_0"]')
        browser.pause(1000)
        browser.click('//li[@class="ds-consent__item"]/div/div/div/div/label[@for="Mobile--19780--Yes_0"]')
        browser.pause(2000)

        browser.scroll('//div[@class="ds-tnc"]');
        browser.waitForVisible('//div[@class="ds-tnc"]');
        browser.click('//div[@class="ds-tnc"]/div[2]/ul/li/div/div/div/div')
        browser.pause(10000)
        console.log(' terms and conditons');
        browser.scroll('//div[@class="ds-tnc"]/div/div/span/span');
        browser.waitForVisible('//button[@class="ds-btn ds-btn--large ds-btn--primary"]');
        browser.click('//button[@class="ds-btn ds-btn--large ds-btn--primary"]')
        browser.pause(5000)
        console.log(' clicked on final confirm and pay button');
        browser.pause(20000);

        browser.click('//div[@class="UXFWidget"]/div/input');
        browser.getUrl();

        browser.pause(25000);


    })



    afterEach(function () {

    })

})
