/** UI TEST VFIT */

const expect = require('chai').expect;
CodiceFiscale = require('codice-fiscale-js/dist/codice.fiscale.commonjs2').CodiceFiscale

describe('Flow Test', function () {

  /**
   * Function opens plan list and clicks activateNow
   * possible to defer the end of the process using a promise.
   * @param {Object} exitCode 0 - success, 1 - fail
  */

  function openPlanList(browser) {
    var link = "http://ilrtvit315:25050/content/vodafone/it/self-service-channel/bundle/home-internet/tv/list.html";
    browser.url(link);
    console.log(" ============================");
    console.log("You are using env link - " + link);
    console.log(" ============================");
    browser.click('#closeicon')
    browser.waitForText('//div[@class="ds-container"]/div/div/div[contains(.,"IperFibra")]');
  }

  function checkServiceability(browser) {
    openPlanList(browser)
    browser.pause(8000)
    console.log("== Serviceability check ==" );
    //browser.click('#closeicon')
    browser.waitForExist('//div[@class="UXFWidget"]/div/div/div/div/div/h1/span[text()="Verifica quali offerte sono disponibili nella tua zona"]');
    browser.pause(1000)
    console.log("== Click on check serviceability button =");
    browser.click('//div[@class="UXFWidget"]/div/div/div/div/div/button');
    browser.pause(8000)
    browser.scroll('//div[@class="UXFWidget"]/div/div/div/div/div/h1/span[text()="Verifica quali offerte sono disponibili nella tua zona"]');

    browser.setValue('//div[@class="ds-container-fluid "]/div/div/div/div/div/input[@name="address.street"]',"via affamata")
    browser.pause(8000)
    browser.setValue('//div[@class="ds-container-fluid "]/div/div/div/div/div/input[@name="address.streetNumber"]',"12312")
    browser.pause(8000)
    console.log("select province and city");
    browser.click('//div[section[input[@name="address.stateOrProvince"]]]')
    browser.waitForVisible('li[value="AG"]')
    browser.click('li[value="AG"]')
    browser.pause(10000)
    browser.click('//div[section[input[@name="address.city"]]]')
    browser.waitForVisible('li[value="SAMBUCA DI SICILIA"]')
    browser.click('li[value="SAMBUCA DI SICILIA"]')
    console.log("Enter postal code");
    browser.keys("\t15015")
    browser.click('//div[@class="col-xs-12 col-sm-4 col-lg-4 radio-group"]/div/div/div/div/label[@for="no_vodafone_label_1"]')

    browser.pause(1000)
    browser.click('//div[@class="col-xs-12 col-sm-12 col-lg-6 radio-group"]/div/div/div/div/label[@for="do_not_transfer_1"]')
    browser.pause(3000)
    browser.click('//button[@type="submit"]/span[text()="Verifica disponibilità"]')
  }

  function planListSelectPlanDiscoverMore(browser, planName) {
    console.log("== planListSelectPlanAcivateNow " + planName + " ==" );
    openPlanList(browser)
    browser.pause(15000)

    console.log("Vodafone one plan list Page")
    console.log(browser.getText('//h3'))

    console.log("Click Discover More")


    browser.scroll('//h3[@title="'+planName+'"]')
    browser.click('//li[contains(.,"' + planName + '")]//button[*="Discover More"]')
    browser.pause(5000)
  }

  function selectPlanAfterServiceabilityCheck(browser, planName){
    console.log("=======================================");
    console.log("== select buy now of serviceable offer - " + planName + " ==" );
    console.log("=======================================");
    browser.pause(7000)

    console.log("Vodafone one plan list Page")
    console.log(browser.getText('//h3'))

    console.log("Click Buy Now")
    browser.pause(5000)
    browser.click('//div[contains(.,"' + planName + '")]//button[*="Acquista ora"]')
    console.log("=======================================");
    console.log("moving to product config")
    browser.pause(18000)
    console.log(planName + " selected and moving to product config");
    console.log("=======================================");
  }

  function selectDevice(browser, deviceName){
    console.log("== selecting the device - " + deviceName + " ==" );
    browser.click('#closeicon')
    browser.pause(3000);
    browser.scroll('div[class="ds-panel__header"]')

    browser.pause(10000);
    console.log("=======================================");
    console.log("verifying number of devices");
    console.log(browser.getText('//div[@class="ds-select-device-item "]'));
    console.log("=======================================");
    expect(browser.getText('//div[@class="ds-select-device-item "]').length) >= (1);

    console.log("selecting device.")
    browser.pause(2000);
    browser.click('//div[@class="ds-select-device-item__wrapper"]/div[contains(.,"' + deviceName + '")]//button[*="Select"]')
    browser.pause(3000)
    browser.waitForVisible('//h2[contains(.,"' + deviceName + '")]')
    browser.click('//button[@class="ds-btn ds-btn--primary"]/span/span')
    browser.pause(15000);
  }

  function randomText() {
    var text = "";
    var possible = "BCDFGHJKLMNPQRSTVWXYZ";
    for (var i = 0; i < 3; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  var firstName = randomText();
  var secondName = randomText();
  function createCustomerWithFicscalCode() {
    var un = new Date().getTime()
    var cf = new CodiceFiscale({
      name: firstName,
      surname: secondName,
      gender: "M",
      day: 24,
      month: 7,
      year: 1957,
      birthplace: "Francia",
      birthplaceProvincia: "EE"
    });
    console.log("=======================================");
    console.log("== createCustomerWithFicscalCode " + un + " ==");
    console.log("=======================================");
    return cf;
  }


  beforeEach(function () {
    var caps = browser.session();
    console.log(browser.session('get').sessionId); // throws an error
    console.log("== ======================= ==");
    console.log("== Clean cache and Cookies ==");
    openPlanList(browser);
    browser.waitForText('//div[@class="ds-container"]/div/div/div[contains(.,"IperFibra")]');
    browser.deleteCookie()
    browser.sessionStorage('DELETE');
    browser.windowHandleSize({ width: 1300, height: 1000 });

  })

  //******** test flow
  it('IperFibra test', function () {
    checkServiceability(browser);
    selectPlanAfterServiceabilityCheck(browser, "Fibra & TV Sport")
    
    browser.waitForVisible('#closeicon', 5000)
    browser.click('#closeicon')
    browser.pause(5000);

    console.log("Click continue on product configuraiton page.");
    browser.click('//div[@class="ds-sticky-footer__action-btn"]/button[contains(.,"Continua")]')

    console.log("Moving to shopping cart page");
    browser.waitForVisible('#closeicon', 15000)
    browser.click('#closeicon')
    console.log("=======================================");
    console.log("Click continue on shopping cart page");
    console.log("=======================================");
    browser.pause(10000)
    browser.click('div[class=ds-sticky-footer__action-btn] > button')
    console.log("Checkout Page")

    browser.pause(15000)
    browser.waitForVisible('#closeicon',5000)
    browser.click('#closeicon')
    console.log("Contact Fiscal Code")
    browser.waitForVisible('input[name="owningIndividual.firstName"]')

    var cf = createCustomerWithFicscalCode()
    console.log(cf);

    browser.scroll("input[name='owningIndividual.firstName']");
    browser.setValue("input[name='owningIndividual.firstName']", cf.name)
    browser.setValue("input[name='owningIndividual.lastName']", cf.surname)
    browser.setValue("input[name='owningIndividual.fiscalCode']", cf.code)
    browser.keys("\t\t\t\t")

    browser.pause(2000)
    console.log(browser.getText('li[value="2"]'))
    browser.click('li[value="2"]')

    browser.setValue("input[name='owningIndividual.identification.identificationNumber']", new Date().getTime())
    browser.keys("\t")
    browser.pause(2000)
    browser.waitForExist('li[value="IT"]')
    console.log(browser.getText('li[value="IT"]'))
    browser.click('li[value="IT"]')
    browser.pause(15000)
    
    console.log($$('div[class="ds-create-customer ds-container-fluid"] > div > div > div > section > div > div > button'));
    browser.click('div[class="ds-create-customer ds-container-fluid"] > div > div > div > section > div > div > button')
    browser.pause(5000)
    console.log("=======================================");
    console.log("Contact Detail Email")
    console.log("=======================================");
    browser.setValue("input[name='owningIndividual.email.emailAddress']", firstName + "@" + secondName + ".com").keys("\t")
    browser.setValue("input[name='owningIndividual.email.confirmEmailAddress']", firstName + "@" + secondName + ".com").keys("\t")
    browser.setValue("input[name='owningIndividual.phone.phoneNumber']", "335123456")
    browser.keys("\t")

    console.log("=======================================");
    console.log("after phone number")
    console.log("=======================================");
    browser.click('li[value="M"]')
    browser.keys("\t")
    browser.pause(8000)
    console.log("Preferred contact time")
    console.log($$('//div[section[input[@name="owningIndividual.preferredContactTime.name"]]]//li'))

    browser.click('li[value="Morning"]')
    browser.keys("\t")
    console.log("=======================================");
    console.log("address of the customer")
    console.log("=======================================");
    browser.setValue("input[name='address.street']", "HASOVA")
    browser.setValue("input[name='address.streetNumber']", "3091")
    browser.setValue("input[name='address.mailBox']", "mb")

    browser.pause(8000)

    browser.scroll('//div[@class="ds-row"]/div/div/span[text()="Address"]');
    console.log("set Address State")
    browser.doubleClick('//div[section[input[@name="address.stateOrProvince"]]]')
    browser.pause(5000);
    browser.doubleClick('//div[section[input[@name="address.stateOrProvince"]]]')
    browser.click('//div[section[input[@name="address.stateOrProvince"]]]')
    browser.waitForVisible('li[value="AL"]')
    browser.click('li[value="AL"]')
    browser.pause(5000)

    console.log("set Address City")
    browser.doubleClick('//div[section[input[@name="address.city"]]]')
    browser.pause(5000);
    browser.doubleClick('//div[section[input[@name="address.city"]]]')
    browser.click('//div[section[input[@name="address.city"]]]')
    browser.waitForVisible('li[value="VILLAMIROGLIO"]')
    browser.click('li[value="VILLAMIROGLIO"]')
    browser.pause(8000)
    console.log("set Address Postal")
    browser.keys("\t15009")
    browser.pause(2000)

    console.log("Continue...")
    console.log($$('//div[@class="ds-address-widget ds-container-fluid"]//button[@type="submit"]'))
    console.log('clicking on submit button for create customer');
    browser.click('//div[@class="ds-address-widget ds-container-fluid"]//button[@type="submit"]')
    browser.pause(25000)
    console.log('Delivery section starts');
    browser.waitForExist('//div[@class="ds-title col-xs-12"]/span/span',10000)
    browser.scroll('//div[@class="ds-title col-xs-12"]/span/span');
    
    browser.click('//section[@class=" ds-row btn-wrapper"]/div/button')
    console.log("=======================================");
    console.log("=====Confirm and pay section starts--=======");
    console.log("=======================================");

    browser.waitForExist('//span[text()="Conferma e Paga"]')
    browser.pause(5000)
    browser.scroll('//div[@class="ds-confirm-and-pay core-radio"]');
    browser.waitForVisible('//div[@class="ds-form__input--wrapper"]');
    browser.click('//div[@class="ds-form__input--wrapper"]/div')
    browser.scroll('//span[text()="Conferma e Paga"]')
    browser.pause(5000)
    console.log(' clicked on paper bill ');

    browser.waitForVisible('//ul[@class="ds-consent__list"]/li/div');
    browser.click('//div[@class="ds-form__line mandatory"]/div/div')
    browser.pause(1000)
    console.log(' wireless subscription option 1');

    browser.waitForVisible('//ul[@class="ds-consent__list"]/li/div');
    browser.click('//div[@class="ds-form__line mandatory error"]/div/div[1]')
    browser.pause(2000)
    console.log(' option 2');
    browser.scroll('//ul[@class="ds-consent__list"]/li/div')

    browser.waitForVisible('//ul[@class="ds-consent__list"]/li/div');
    
    browser.pause(2000)
    console.log("=======================================");
    console.log("vodafone happy consent selected");
    console.log("=======================================");

    browser.scroll('//div[@class="ds-tnc"]');
    browser.waitForVisible('//div[@class="ds-tnc"]');
    browser.click('//div[@class="ds-tnc"]/div[2]/ul/li/div/div/div/div')
    browser.pause(5000)
    console.log(' terms and conditons');
    browser.scroll('//div[@class="ds-tnc"]/div/div/span/span');
    browser.waitForVisible('//button[@class="ds-btn ds-btn--large ds-btn--primary"]');
    browser.click('//button[@class="ds-btn ds-btn--large ds-btn--primary"]')
    browser.pause(5000)
    console.log(' final confirm and pay');
    browser.pause(20000);

    browser.click('//div[@class="UXFWidget"]/div/input')
    browser.pause(8000)

  })



  afterEach(function () {

  })

})
