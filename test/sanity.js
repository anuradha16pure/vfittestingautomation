/** UI TEST VFIT */


const assert = require('chai').assert;
const expect = require('chai').expect;
CodiceFiscale = require('codice-fiscale-js/dist/codice.fiscale.commonjs2').CodiceFiscale
var testdata = require('../testdata/sanity.json')
var env_details = require('../testdata/env_config.json')


describe(testdata.TestSuite, function () {

  /**
   * Function opens plan list and clicks activateNow
   * possible to defer the end of the process using a promise.
   * @param {Object} exitCode 0 - success, 1 - fail
  */

 beforeEach(function () {
  console.log("Before each started");
  var caps = browser.session();
  console.log(browser.session('get').sessionId); // throws an error
  console.log("== ======================= ==");
  console.log("== Clean cache and Cookies ==");
  //openPlanList(browser);              
  //browser.sessionStorage('DELETE');
  browser.windowHandleFullscreen();
  console.log("Before each ended");
  browser.pause(5000)
  openEnv(browser)
  browser.pause(15000)
  close_banner(browser)

})

function close_banner(browser) {
  //Close banner
  //openEnv(browser)
  browser.waitForExist('#closeicon')
  browser.click('#closeicon')
  browser.pause(2000)
  
}

function openEnv(browser) {

  browser.url(env_details.ENVurl);
  
      console.log(" ============================");

      console.log("You are using env link - "+env_details.ENVurl);

      console.log(" ============================");

      
}

function Verify_planlist_page(browser) {

  browser.pause(2000)
 
  browser.scroll('//li[contains(.,"' + testdata.PlanName[0] + '")]')

  //verify activate now exist
  
  browser.waitForExist('//li[contains(.,"' + testdata.PlanName[0] + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')

  browser.pause(2000)

  //search plan 1 & 2
  
  search_plans(browser,testdata.searchInput[0],testdata.PlanName[1])
  
  browser.pause(2000)

  search_plans(browser,testdata.searchInput[1],testdata.PlanName[2])

  browser.pause(5000)

  openEnv(browser)

  browser.pause(15000)

  filter_plans(browser,testdata.FilterWithPara[0])

  browser.pause(5000)
 
}

function search_plans(browser,searchInput,planName){
  var searchplan = searchInput;
  var VerifyPlan = planName;

  browser.pause(2000)

  //Search is displayed

  browser.waitForExist('//div[@class="ds-search"]')
  browser.setValue('//div[@class="ds-search"]/div/input',searchplan)
  browser.scroll('//div[@class="ds-search"]')
  browser.pause(2000)
  browser.keys("Enter")
  browser.pause(2000)    
  browser.waitForVisible('//div[@class="UXFWidget"]//h3')
  browser.pause(2000)
  assert.strictEqual(browser.getText('//section[@class="plan"]//h3'),VerifyPlan)
  console.log("Plan is searched")
  
}

function filter_plans(browser,filtername){


  browser.scroll('//div[@class="ds-search"]')

  browser.waitForVisible('//div[@class="UXFWidget"]//h3')

  browser.pause(2000)

  browser.click('//div[@data-for="moreFilters"]')

  browser.click('//div[@class="ds-dropdown open"]/div[2]//label/span[contains(.,"'+filtername+'")]')
  browser.click('//div[@class="ds-dropdown open"]/div[2]//button[contains(.,"Applica")]')
  browser.pause(2000)
  browser.waitForVisible('//div[@class="UXFWidget"]//h3')
  browser.pause(2000)
  assert.equal(browser.getText('//div[@class="ds-plans"]//li[1]//h3'),testdata.PlanName[2])
  //expect((browser.getText('//div[@class="ds-plans"]//li[1]//h3')).to.contain.toString(testdata.PlanName[1]))
   

 }
 




function check_compare_checkbox(browser, planName){

  var plannameme = planName
  browser.click('//li[contains(.,"' + plannameme + '")]//div/div[1]/div[2]/div/div')
}

function Compare_plans(browser, planName1, planName2) {

    console.log("Plan list Page")
    console.log("==Testing Plan list page layout")

    browser.waitForVisible('//section[@class="plan"]/section/div/ul/li[1]')
    
    console.log("Below plans are displayed...");

    console.log(browser.getText('//h3'))

    //3. Verify that User has an ability to view plan list and activate the plans. 

    browser.waitForExist('//li[contains(.,"' + planName1 + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')

    // 4. Verify that user can see more about the plans. (Discover plans)

    browser.waitForExist('//li[contains(.,"' + planName2 + '")]//button[*="'+ testdata.VerifyText[0].Discover_More_Button_Name+'"]')
    
    //******** Compare check box is present

    //5. Verify that user can select plans for comparision (Compare check box).


    browser.scroll('//ul/li[1]//div[@class="generic-item__header"]')

    browser.pause(1000)


    //********* Select a plan 1 for comparision 

    browser.waitForVisible('//li[contains(.,"' + planName1 + '")]')

    check_compare_checkbox(browser,planName1)


    console.log("Added " +planName1+ " for comparision...")

     //check plan is displayed in footer
     browser.waitForVisible('//div[@class="UXFWidget"]//h2[contains(.,"' +planName1 + '")]')

     console.log(planName1+ " is displayed in the sticky footer")

    //check compare is disabled
    browser.waitForVisible('//div[@class="UXFWidget"]//button[@class="ds-btn ds-btn--small ds-btn--primary disabled"]')

    console.log(" Compare button is disabled")

    //********* Select a plan 2 for comparision 
    browser.pause(1000)

    check_compare_checkbox(browser,planName2)


    console.log("Added " +planName2+ " for comparision...")

    //check compare is enabled
    browser.waitForVisible('//div[@class="UXFWidget"]//button[@class="ds-btn ds-btn--small ds-btn--primary"]')

    console.log(" Compare button is enabled")

    //Remove plans from comparision
    check_compare_checkbox(browser,planName1)

    browser.pause(1000)

    check_compare_checkbox(browser,planName2)

    browser.pause(1000)

   //Verify that sticky footer disappears...

    var exist = browser.waitForExist('//div[@class="UXFWidget"]//button[@class="ds-btn ds-btn--small ds-btn--primary"]',500,true)
    if(exist = true){
      console.log(" Sticky footer not displayed")
    }


     //Add plans from comparision
   

     browser.pause(1000)

     check_compare_checkbox(browser,planName1)

    browser.pause(1000)

    check_compare_checkbox(browser,planName2)

    browser.pause(1000)

    // Click on compare button

    browser.click('//div[@class="UXFWidget"]//button[contains(.,"'+ testdata.VerifyText[0].Compare_Button_Name+'")]')

}




function compare_plan_page(browser,planName1,planName2) {

  //Verify user is redirected to Compare page

  browser.pause(10000)

  //close_banner(browser)

  browser.waitForExist('//div[@class="ds-compare-device__header"]')

  

  expect('//span[@class="ds-title__text"]/span').to.contain.toString( testdata.VerifyText[0].Compare_Plans_Page_Title)

  console.log("Compare page is displayed")


  // Verify Plans are displayed on Compare page

  browser.waitForExist('//ul[@class="device__list plan__list"]/li[contains(.,"' + planName1 + '")]')


  //Verify Plan 1 and plan 2 are getting compared

  browser.waitForExist('//ul[@class="device__list plan__list"]/li[contains(.,"' + planName2 + '")]')

  // Display Comparision parameters

  console.log('Plans are compared on basis of below parameters...')
  
  console.log(browser.getText('//div[@class="ds-row specs"]/div/div'))

  //Verify user has abilility to activate plan and discover more about plans on compare page...

  browser.waitForExist('//ul[@class="device__list plan__list"]/li[contains(.,"' + planName1 + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')

  //Verify that sticky header with plan names is displayed if user scrolls down

  browser.scroll('//ul[@class="device__list plan__list"]/li[contains(.,"' + planName1 + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')

  expect('//div[@class="ds-sticky-header"]//h2').to.contain.toString(planName1)

  expect('//div[@class="ds-sticky-header"]//h2').to.contain.toString(planName2)

  browser.scroll('//button[@class="ds-btn ds-btn--secondary ds-right"]')

  // Click on black to plans and verify user is redirected back to plan list page
  //Beacause of the header the back button is not visible
  //browser.click('//button[@class="ds-btn ds-btn--secondary ds-right"]')
  
  browser.back()
  browser.pause(2000)
 
  browser.scroll('//li[contains(.,"' + testdata.PlanName[0] + '")]')

  //verify activate now exist
  
  browser.waitForExist('//li[contains(.,"' + testdata.PlanName[0] + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')

}

function ClickDiscoverNow(browser, planName) {

  browser.pause(2000)

  browser.scroll('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
  
  browser.waitForExist('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Discover_More_Button_Name+'"]')
  
  browser.click('//section[@class="plan"]//li[contains(.,"'+planName+'")]//button[contains(.,"'+testdata.VerifyText[0].Discover_More_Button_Name+'")]')
  
  browser.pause(15000) 
}

function Add_Add_Ons(browser) {

  browser.pause(15000)

  browser.waitForExist('//h1[contains(.,"'+testdata.VerifyText[0].On_top_of_your_plan+'")]')

  browser.scroll('//h1[contains(.,"'+testdata.VerifyText[0].On_top_of_your_plan+'")]')
 
 browser.click('//div[@class="ds-addon--item"]/div[contains(.,"'+testdata.VerifyText[0].ADD_On_Name+'")]//button[contains(.,"'+testdata.VerifyText[0].Click_On_ADD+'")]')

  browser.pause(5000)

  console.log('clicked Add')

  browser.pause(5000)

  
 }

 function shopping_cart(browser){

  //Remove Add on

  browser.debug()

  browser.click('//span[@class="ds-icon ds-icon-trash_blue"]')

  browser.pause(5000)

  // Click on the Remove button on the pop up

  browser.click('//button[contains(.,"'+testdata.VerifyText[0].remove+'")]')

  browser.pause(20000)

  browser.waitForExist('//span[@class="ds-icon ds-icon-trash"]')

  // Click on the empty cart icon

  browser.click('//span[@class="ds-icon ds-icon-trash"]')

  browser.pause(5000)

  // Click on the empty cart button on the pop up

  browser.click('//button[contains(.,"'+testdata.VerifyText[0].empty_cart+'")]')

  browser.pause(2000)

  // Verify that the empty cart image is displayed on the shopping cart page

  browser.waitForExist('//div[@class="ds-empty-cart__image"]')

}

function openPlanListAndClickActivateNow(browser, planName) {

  browser.pause(2000)
 
  browser.scroll('//li[contains(.,"' + planName + '")]')
  
  browser.waitForExist('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
  
  browser.click('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
  
  browser.pause(10000)
 
}

function ClickonCheckout(browser) {
  browser.pause(10000)
  browser.waitForExist('//button[*="'+testdata.VerifyText[0].Checkout_Btn_Name+'"]')
  browser.click('//button[*="'+testdata.VerifyText[0].Checkout_Btn_Name+'"]')

}

function randomText() {
  var text = "";
  //Here vowels are taken into possible just to make the generation of fiscal code easier.
  var possible = "BCDFGHJKLMNPQRSTVWXYZ";
  for (var i = 0; i < 3; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}



var firstName = randomText();
var secondName = randomText();

function createCustomerWithFicscalCode() {
  var un = new Date().getTime()
  var cf = new CodiceFiscale({
    name: firstName,
    surname: secondName,
    gender: "M",
    day: 24,
    month: 7,
    year: 1992,
    birthplace: "Francia",
    birthplaceProvincia: "EE"
  });
  console.log("== createCustomerWithFicscalCode " + un + " ==");
  return cf;
}

function FillPersonalInfo(browser) {

  var cf = createCustomerWithFicscalCode()
  var document_num = cf.code;
  //browser.debug()
  browser.waitForExist('//input[@name="owningIndividual.firstName"]')
  var Pri_contact_name = cf.name + ' '+cf.surname;
  console.log(Pri_contact_name)
  
  //Set first name
  browser.setValue("input[name='owningIndividual.firstName']", cf.name)
  
  //Set last name
  browser.setValue("input[name='owningIndividual.lastName']", cf.surname)
  
  browser.keys("\t")
  
  browser.pause(2000)
  
  console.log(browser.getText('li[value="IT"]'))
  browser.click('li[value="IT"]')
  
  browser.keys("\t")
  //Set fiscal code
  browser.setValue("input[name='owningIndividual.fiscalCode']", cf.code)
  browser.scroll('input[name="owningIndividual.identification.identificationNumber"]')
  browser.keys("\t\t\t\t")
  browser.pause(10000)
  // Set Issuing nation
  browser.waitForExist('li[value="'+testdata.VerifyText[0].Document_dropdown_value+'"]')
  console.log(browser.getText('li[value="'+testdata.VerifyText[0].Document_dropdown_value+'"]'))
  browser.click('li[value="'+testdata.VerifyText[0].Document_dropdown_value+'"]')
  // Set value in document number
  browser.setValue("input[name='owningIndividual.identification.identificationNumber']", document_num)
  browser.keys("\t")
  browser.pause(2000)
  console.log(browser.getText('//div[section[input[@name="owningIndividual.identification.nationality"]]]//li[@value="IT"]'))
  browser.click('//div[section[input[@name="owningIndividual.identification.nationality"]]]//li[@value="IT"]')
  browser.keys("\t")
  //Because of a defect not setting default date remove it after the defect fix
  //browser.keys("Delete")
  browser.setValue("//div[@class='react-datepicker__input-container']/input","2020-04-10").keys("\t")

  // Click on continue
  browser.click('//div[@class="ds-create-customer ds-container"]//button[contains(.,"Continua")]')
  // Enter contact info
  browser.pause(5000)
  // set contact email id
  // browser.scroll('input[name="owningIndividual.email.emailAddress"]')
  browser.setValue("input[name='owningIndividual.email.emailAddress']", "anu@gmail.com").keys("\t")
  // confirm email id is now removed in 0.7
  // browser.setValue("input[name='owningIndividual.email.confirmEmailAddress']", "anu@gmail.com").keys("\t")
  // Enter phone number
  browser.setValue("input[name='owningIndividual.phone.phoneNumber']", "335123456")
  browser.keys("\t")
  // set preferred contact method
  browser.click('li[value="'+testdata.VerifyText[0].preferred_contact_method+'"]')
  browser.keys("\t")
  // set preferred contact time
  browser.click('li[value="'+testdata.VerifyText[0].preferred_contact_time+'"]')
  // leave preferred language as it is preselected as Italian
  
  
}

function create_address(browser) {

    
  //Step 4 : Enter city
  browser.scroll('//section[@class="address"]')
  browser.setValue('//section[@class="address"]/div[2]//div[1]/div[1]//div[@class="autocomplete-container ds-search"]/input',testdata.VerifyText[0].City_search_text)
  browser.pause(1000)
  // Select 1st from auto-complete options
  browser.waitForExist('//ul[@class="values-list generic-list"]')
  browser.click('//section[@class="address"]/div[2]//div[1]/div[1]//div[@class="autocomplete-container ds-search active"]//ul/li[1]')
  browser.pause(1000)
  // Step 4 : Enter street name
  browser.setValue('//section[@class="address"]/div[2]//div[1]/div[2]//div[@class="autocomplete-container ds-search"]/input',testdata.VerifyText[0].Street_search_text)
  browser.pause(25000)
  // Select 1st from auto-complete options
  browser.waitForExist('//ul[@class="values-list generic-list"]')
  browser.click('//ul[@class="values-list generic-list"]//li[1]')
  // Set house number
  browser.setValue("input[name='address.streetNumber']", "99")
  /*
  // Set value in mailbox
  browser.setValue("input[name='address.mailBox']","mailbox")
  // Set value in frazione
  browser.setValue("input[name='address.frazione']","frazione")
  */
  // Step 5 : Click on continue
  browser.pause(2000)
  browser.click('//button[contains(.,"Continua")]')
  browser.pause(40000)
  //verify edit mode
  browser.waitForExist('//span[@class="ds-icon ds-icon-personal--dark"]')
  //browser.click('//span[contains(.,"edit")]')
}

function randomPhoneNum() {
  var phoneNumber = "35";
  var possible = "25154565";
  for (var i = 0; i < 8; i++)
  phoneNumber += possible.charAt(Math.floor(Math.random() * possible.length));
  return phoneNumber;
}

function set_PhoneNumber (browser) {

  // If port in selected 

  if(testdata.VerifyText[0].want_to_portin = "yes"){

    console.log('you are in port in loop')

    //click on keep my number port in button
    //browser.click('//input[@id="portInOption1_1"]') // //label[@for='portInOption1_1']

    browser.click('//label[@for="portInOption1_1"]') 
    browser.scroll('//label[@for="portInOption1_1"]')
    var portInMSISDN = randomPhoneNum()
    console.log(portInMSISDN)
    //console.log(portInMSISDN)
    //Enter current MSISDN
    browser.setValue("//input[@id='phoneNumber']",portInMSISDN).keys('\t')
    //Click on service provider dropdown
    //browser.click('//input[@name="currentProvider"]')
    //select your service provider
    browser.click('//li[contains(.,"'+testdata.VerifyText[0].Service_provider+'")]')
    //Set sim number else check the checkbox
    if(testdata.VerifyText[0].haveSIMnumber = "yes"){
    browser.setValue("//input[@id='simNumber']",testdata.VerifyText[0].SIM_number)
    }
    else{
      //browser.scroll('//input[@id="phoneNumber"]')
      browser.click('//input[@name="simAvailable"]')
    }
    //Click on radio button of subscription / Rechargable
    //input[@id='Subscription_0'] Rechargeable
    if(testdata.VerifyText[0].click_subscription_rechagable = "Subscription_0"){
      browser.click('//label[@for="'+testdata.VerifyText[0].click_subscription_rechagable+'"]')
    }
    else{
      browser.click('//label[@for="'+testdata.VerifyText[0].click_subscription_rechagable+'"]')
      if(testdata.VerifyText[0].rechargable_checkbox_value = "checked"){
      
      }
  
      else {
        browser.click('//input[@name="transferRemainingBalance"]')
      }
    }
     //Click on continue 
     browser.click('//button[contains(.,"Continua")]')
  }

  // If user wants new vodafone number
  else{

    // Step 6 : Phone Number Section 
    browser.waitForExist('//label[@id="portInOption2_0"]/span[@class="ds-form__icon"]')
    //Check by default your number is... radio button selected
    //Click on select new voda number radio button
    browser.waitForExist('//label[@for="retainVodafoneNumber_0"]//span[@class="ds-form__design"]|before')
    //if he wants to select from default numbers
    if(testdata.VerifyText[0].select_first_default_number = "yes"){
        
      browser.click('//label[@for="newVodafoneNumber_1"]')
      browser.waitForExist('//div[@class="ds-title__small"]')
      //Click on the first number out of 6 numbers
      browser.click('//li['+testdata.VerifyText[0].select_nth_default_number +']//input[@name= "phoneNumber"]')
    }
    //if he wants to select default numbers
    else{

    //Click on continue 
    browser.click('//button[contains(.,"Continua")]')
    
    }

  }
  //verify successfully submitted
  browser.pause(20000)
  browser.scroll('//div[@class="ds-info-box review"]')
  browser.waitForExist('//span[@class="ds-icon ds-icon-phone-number"]')
  //Verify if temporary number is being allocated.
  browser.waitForExist('//div[@class="note-text"]')
}

var firstName_1 = randomText();
var secondName_1 = randomText();

function createContactWithFicscalCode() {
  var un = new Date().getTime()
  var cf = new CodiceFiscale({
    name: firstName_1,
    surname: secondName_1,
    gender: "F",
    day: 24,
    month: 7,
    year: 1980,
    birthplace: "Francia",
    birthplaceProvincia: "EE"
  });
  console.log("== createContactWithFicscalCode " + un + " ==");
  return cf;
}

function create_contact (browser) {

  
  var cf_1 = createContactWithFicscalCode()
  var document_num = cf_1.code;
  var Sec_contact_name = cf_1.name + ' '+cf_1.surname;
  console.log(Sec_contact_name)
  //browser.debug()
  browser.waitForExist('//input[@name="firstName"]')
  //Set first name
  browser.setValue("input[name='firstName']", cf_1.name)
  
  //Set last name
  browser.setValue("input[name='lastName']", cf_1.surname)
  
  browser.keys("\t")
  
  browser.pause(2000)
  
  console.log(browser.getText('li[value="IT"]'))
  browser.click('li[value="IT"]')
  
  browser.keys("\t")
  //Set fiscal code
  browser.setValue("input[name='fiscalCode']", cf_1.code)
  browser.scroll('input[name="identification.identificationNumber"]')
  browser.keys("\t\t\t\t")
  browser.pause(2000)
  // Set Issuing nation
  browser.waitForExist('li[value="'+testdata.VerifyText[0].Document_dropdown_value+'"]')
  console.log(browser.getText('li[value="'+testdata.VerifyText[0].Document_dropdown_value+'"]'))
  browser.click('li[value="'+testdata.VerifyText[0].Document_dropdown_value+'"]')
  // Set value in document number
  browser.setValue("input[name='identification.identificationNumber']", document_num)
  browser.keys("\t")
  browser.pause(2000)
  console.log(browser.getText('//div[section[input[@name="identification.nationality"]]]//li[@value="IT"]'))
  browser.click('//div[section[input[@name="identification.nationality"]]]//li[@value="IT"]')
  browser.keys("\t")
  //Because of a defect not setting default date remove it after the defect fix
  //browser.keys("Delete")
  browser.setValue("//div[@class='react-datepicker__input-container']/input","2020-04-10")
  browser.keys("\t")
  // Enter contact info
  browser.pause(5000)
  // set contact email id
  // browser.scroll('input[name="owningIndividual.email.emailAddress"]')  
  browser.setValue("input[name='emailAddress']", "anu@gmail.com").keys("\t")
  
  // confirm email id is now removed in 0.7
  // browser.setValue("input[name='owningIndividual.email.confirmEmailAddress']", "anu@gmail.com").keys("\t")
  // Enter phone number
  browser.setValue("input[name='phoneNumber']", "335123445")
  browser.debug()
  /*
  browser.keys("\t")
  //browser.scroll('li[value="'+testdata.VerifyText[0].preferred_contact_method+'"]')
  // set preferred contact method
  browser.click('li[value="'+testdata.VerifyText[0].preferred_contact_method+'"]')
  browser.keys("\t")
  // set preferred contact time
  browser.click('li[value="'+testdata.VerifyText[0].preferred_contact_time+'"]')
  // leave preferred language as it is preselected as Italian
  */
  
  //Click on ADD 
  browser.pause(1000)
  browser.click('//button[@class="ds-btn ds-btn--large ds-btn--primary"]/span[contains(.,"Aggiungi")]')
  browser.pause(1000)

  return cf_1;
}

function replace_contact (browser) {
  browser.waitForExist('//button[contains(.,"Modifica contatto")]')
  //Click on replace contact
  browser.click('//button[contains(.,"Modifica contatto")]')
  browser.pause(2000)
  //Click on Add Contact
  browser.waitForExist('//span[contains(.,"Associa un contatto al numero")]')
  browser.click('//span[contains(.,"Aggiungi un nuovo contatto")]')
  browser.pause(10000)
  create_contact(browser)
  //Click on Save
  browser.pause(5000)
  //Click on Continue for replace contact
  browser.click('//button[contains(.,"Salva")]')
  var cf_1 = createContactWithFicscalCode()
  var contact_name = cf_1.name + ' '+cf_1.surname;
  console.log(contact_name)
  //browser.debug()
  assert.strictEqual(browser.getText('//span[@class="ds-title__small text-capitalize"]'),contact_name)
  browser.click('//button[contains(.,"Continua")]')
  browser.pause(10000)
  browser.waitForExist('//div[@class="ds-title"]//span[@class="ds-icon ds-icon-contact"]')
  
  }

  function  edit_consents(browser){

  
    browser.scroll('//header//span[@class="ds-title__text"]/span[contains(.,"Pagamento")]')
  
    browser.waitForExist('//span[contains(.,"Gestisci i tuoi consensi")]')
  
    for (let index = 1; index < 5; index++) {
      const element = index;
      
      browser.click('//div[@class="consents"]//label[@for="'+element+'--yes_0"]')
      browser.click('//div[@class="consents"]//label[@for="1--yes_0"]')
    }
    //Open I agree collapse
    browser.click('//button[@class="ds-open"]')
    //Click i agree checkbox
    browser.click('//label[@for="termsAndConditionsAcceptance"]')
    browser.pause(1000)
    browser.waitForExist('//div[@class="btn-wrapper"]/button/span')
    //click pay
    browser.click('//div[@class="btn-wrapper"]/button/span')
    browser.pause(5000)
  
    //wait for existance
    browser.waitForExist('//div[@class="ds-order-confirmation__item__content__icon__mail"]')
  
    }

  function replace_contact_existing(browser){

      browser.waitForExist('//button[contains(.,"Modifica contatto")]')
      //Click on replace contact
      browser.click('//button[contains(.,"Modifica contatto")]')
      browser.pause(2000)
      //Click on Add Contact
      browser.waitForExist('//span[contains(.,"Associa un contatto al numero")]')
      browser.pause(2000)
      browser.click('//span[contains(.,"Aggiungi un nuovo contatto")]')
      browser.pause(10000)
      //create_contact(browser)
    
      //Using the default contact created in every env
      //browser.debug()
      var existing_firstname = "Testing";
      var existing_lastname = "Testing";
      var existing_fiscalename = "TSTTTN91B289999N";
    
    
      browser.waitForExist('//input[@name="firstName"]')
      //Set first name
      browser.setValue("input[name='firstName']", existing_firstname)
      
      //Set last name
      browser.setValue("input[name='lastName']", existing_lastname)
      
      browser.keys("\t")
      
      browser.pause(2000)
      
      console.log(browser.getText('li[value="IT"]'))
      browser.click('li[value="IT"]')
      
      browser.keys("\t")
      //Set fiscal code
      browser.setValue("input[name='fiscalCode']", existing_fiscalename).keys('\t')
      browser.pause(2000)
      //browser.debug()
      // check if exiting contact pop up is displayed
      var iscontact_existing = browser.waitForExist('//header//span[contains(.,"Is this an existing contact?")]');
      if(iscontact_existing == true)
      {
        browser.pause(1000)
        //Click on continue on existing contact pop over
        browser.click('//button[contains(.,"Yes, verify by SMS")]')
        //Click on continue on OTP pop over -- > No functionality as part  of MTV469
        browser.waitForExist('//header[contains(.,"Verifica informazioni")]')
        browser.click('//footer//button[contains(.,"Continua")]')
    
        //Click on ADD 
        browser.pause(7000)
        browser.debug()
        browser.waitForExist('//button[@class="ds-btn ds-btn--large ds-btn--primary"]//span[contains(.,"Aggiungi")]')
        browser.click('//button[@class="ds-btn ds-btn--large ds-btn--primary"]//span[contains(.,"Aggiungi")]')
        //Click on Save
        browser.pause(5000)
        //Click on Continue for replace contact
        browser.click('//button[contains(.,"Salva")]')
        var cf_1 = createContactWithFicscalCode()
        var contact_name = existing_firstname +" "+existing_lastname;
        console.log(contact_name)
        //browser.debug()
        assert.strictEqual(browser.getText('//span[@class="ds-title__small text-capitalize"]'),contact_name)
        browser.click('//button[contains(.,"Continua")]')
        browser.pause(10000)
        browser.waitForExist('//div[@class="ds-title"]//span[@class="ds-icon ds-icon-contact"]')
    
      }
     
      else{
        create_contact(browser)
    
        //Click on Save
        browser.pause(5000)
        //Click on Continue for replace contact
        browser.click('//button[contains(.,"Salva")]')
        var cf_1 = createContactWithFicscalCode()
        var contact_name = cf_1.name +" "+cf_1.surname;
        console.log(contact_name)
        //browser.debug()
        assert.strictEqual(browser.getText('//span[@class="ds-title__small text-capitalize"]'),contact_name)
        browser.click('//button[contains(.,"Continua")]')
        browser.pause(10000)
        browser.waitForExist('//div[@class="ds-title"]//span[@class="ds-icon ds-icon-contact"]')
    
      }   
    
  }

  function allocate_number(browser){
    browser.pause(10000)
    browser.click('//button[contains(.,"Continua")]')
    
  }

  function port_in(browser) {



    console.log('you are in port in loop')
  
      //click on keep my number port in button
      browser.click('//label[@for="portInOption1_1"]') 
      browser.scroll('//label[@for="portInOption1_1"]')
    
      var portInMSISDN = randomPhoneNum()
      
      //Enter current MSISDN
      browser.setValue("//input[@id='phoneNumber']",portInMSISDN).keys('\t')

      browser.click('//section/ul/li[1]')
     
      //select your service provider
      //browser.click('//li[contains(.,"'+testdata.VerifyText[0].Service_provider+'")]')
    
      //Set sim number else check the checkbox


    
      if(testdata.VerifyText[0].haveSIMnumber = "yes"){
    
      browser.setValue("//input[@name='simNumber']",testdata.VerifyText[0].SIM_number)
      }
    
      else{
        
        browser.click('//input[@name="simAvailable"]')
      }
    
      //Click on radio button of subscription / Rechargable
      if(testdata.VerifyText[0].click_subscription_rechagable = "Subscription_0"){
    
        browser.click('//label[@for="'+testdata.VerifyText[0].click_subscription_rechagable+'"]')
      }
    
      else{
    
        browser.click('//label[@for="'+testdata.VerifyText[0].click_subscription_rechagable+'"]')
        if(testdata.VerifyText[0].rechargable_checkbox_value = "checked"){
        
        }
    
        else {
          browser.click('//input[@name="transferRemainingBalance"]')
        }
      }
       //Click on continue 
     browser.click('//button[contains(.,"Continua")]')
     
     browser.pause(5000)
     //wait for edit mode for phone number
    browser.waitForExist('//span[@class="ds-icon ds-icon-phone-number"]')
    //Verify that temparory number is alllocated.
    browser.waitForExist('//span[contains(.,"Please note: ")]')
    }

    function set_New_PhoneNumber (browser) {


      // If user wants new vodafone number
    
    
        // Step 6 : Phone Number Section 
        browser.waitForExist('//label[@for="portInOption2_0"]/span[@class="ds-form__icon"]')
        //Check by default your number is... radio button selected
        //Click on select new voda number radio button
        browser.waitForExist('//label[@for="retainVodafoneNumber_0"]//span[@class="ds-form__design"]|before')
        //if he wants to select from default numbers
        if(testdata.port_in_info[0].select_first_default_number = "yes"){
            
          browser.click('//label[@for="newVodafoneNumber_1"]')
          browser.waitForExist('//div[@class="ds-title__small"]')
          //Click on the first number out of 6 numbers
          browser.click('//li['+testdata.VerifyText[0].select_nth_default_number +']//input[@name= "phoneNumber"]')
        }
        //if he wants to select default numbers
        else{
    
        //Click on continue 
        browser.click('//button[contains(.,"Continua")]')
        
        }
    
      
      //verify successfully submitted
      browser.pause(20000)
      browser.scroll('//div[@class="ds-info-box review"]')
      browser.waitForExist('//span[@class="ds-icon ds-icon-phone-number"]')
      //Verify if temporary number is being allocated.
      browser.waitForExist('//div[@class="note-text"]')
  }

  function contact_continue(browser){
    browser.pause(10000)
    //wait for edit mode for phone number
    browser.waitForExist('//span[@class="ds-icon ds-icon-phone-number"]')
    browser.click('//button[contains(.,"Continua")]')
    browser.pause(5000)
    browser.waitForExist('//span[@class="ds-icon ds-icon-contact"]')
  }



  function select_shake_and_remix(browser){

    browser.pause(1000)
    browser.waitForExist('//div[@data-uxfwidget-id="digitalexpshakenremi"]//button[*="'+ testdata.VerifyText[0].Discover_More_Button_Name+'"]')
    browser.click('//div[@data-uxfwidget-id="digitalexpshakenremi"]//button[*="'+ testdata.VerifyText[0].Discover_More_Button_Name+'"]')
    browser.pause(10000)
    browser.scroll('//button[contains(.,"Continua")]')
    
  
    for (i = 1; i < 4; i++) { 
         
        var form = $('//div[@class="shake-and-remix-container"]/div['+i+']/div/button[1]')
        var isDisabled = form.getAttribute('class')
        console.log(isDisabled)
        
        if (isDisabled !='slick-arrow slick-next slick-disabled')
        {
        
          console.log('//div[@class="shake-and-remix-container"]/div['+i+']/div/button[1]')
          browser.click('//div[@class="shake-and-remix-container"]/div['+i+']/div/button[1]')
        
        
        }

        else{
       
        console.log('//div[@class="shake-and-remix-container"]/div['+i+']/div/button[2]')
        browser.click('//div[@class="shake-and-remix-container"]/div['+i+']/div/button[2]')

        }
          
      }



      browser.pause(2000)
      
      browser.click('//button[contains(.,"Continua")]')

      
  }

  function click_continue_product_config(browser){
    //Attiva online

    browser.pause(10000) 
    browser.waitForExist('//button[contains(.,"Attiva online")]')
    browser.click('//button[contains(.,"Attiva online")]')
  }

/*

it(testdata.TestCaseName1, function () {

  // Step 1 : Select a plan and click on activate now
  Verify_planlist_page(browser)

})




it(testdata.TestCaseName2, function () {
  
  
  
  Compare_plans(browser,testdata.PlanName[1],testdata.PlanName[2])

  compare_plan_page(browser,testdata.PlanName[1],testdata.PlanName[2])
  
  
})


 
 it(testdata.TestCaseName3, function () {

  // Step 1 : Select a plan and click on Discover now
  ClickDiscoverNow(browser, testdata.PlanName[2])

  browser.pause(25000)

  // Step 2 : Click on Checkout button
  Add_Add_Ons(browser)

  browser.pause(25000)
  // Step 3 : Click Activate Now
  browser.click('//span[contains(.,"'+testdata.VerifyText[0].Activate_Now_Button_Name+'")]')

  browser.pause(5000)

  // Step 4 : Test Shopping cart
  shopping_cart(browser)
  //browser.click('//span[@class="ds-icon ds-icon-trash_blue"]')
  
  
  
})

*/




it(testdata.TestCaseName4, function () {

 // Step 1 : Select a plan and click on activate now
 //openPlanListAndClickActivateNow(browser, testdata.PlanName[2])
 // Step 2 : Click on Checkout button
 //ClickonCheckout(browser)
 //browser.pause(10000)
 browser.debug()
 // Step 3 : Insert Personal Information and Contact Information
 FillPersonalInfo(browser)
 // Step 4 : Personal Info And Contact Info
 create_address(browser)
 // Step 5 : Select phone number
 allocate_number(browser)
 // Step 5 : Select Contact to associate
 contact_continue(browser)
 // Step 6 : Edit Consents
 edit_consents(browser)

})


/*


it(testdata.TestCaseName5, function () {

  // Step 1 : Select a plan and click on activate now
  openPlanListAndClickActivateNow(browser, testdata.PlanName[2])
  browser.pause(25000)
  // Step 2 : Click on Checkout button
  ClickonCheckout(browser)
  browser.pause(25000)
  // Step 3 : Insert Personal Information and Contact Information
  FillPersonalInfo(browser)
  // Step 4 : Personal Info And Contact Info
  create_address(browser)
  // Step 5 : Select phone number
  port_in(browser)
  // Step 5 : Select Contact to associate
  contact_continue(browser)
  // Step 6 : Edit Consents
  edit_consents(browser)
  
 })
  
 



 it(testdata.TestCaseName6, function () {

  // Step 1 : Select a plan and click on activate now
  openPlanListAndClickActivateNow(browser, testdata.PlanName[2])
  browser.pause(25000)
  // Step 2 : Click on Checkout button
  ClickonCheckout(browser)
  browser.pause(25000)
  // Step 3 : Insert Personal Information and Contact Information
  FillPersonalInfo(browser)
  // Step 4 : Personal Info And Contact Info
  create_address(browser)
  // Step 5 : Select phone number
  //set_New_PhoneNumber(browser)
  allocate_number(browser)
  // Step 5 : Select Contact to associate
  replace_contact_existing(browser)
  // Step 6 : Edit Consents
  edit_consents(browser)
  
 })


 
 it(testdata.TestCaseName7, function () {

  // Step 1 : Select a plan and click on activate now
  openPlanListAndClickActivateNow(browser, testdata.PlanName[2])
  browser.pause(25000)
  // Step 2 : Click on Checkout button
  ClickonCheckout(browser)
  browser.pause(25000)
  // Step 3 : Insert Personal Information and Contact Information
  FillPersonalInfo(browser)
  // Step 4 : Personal Info And Contact Info
  create_address(browser)
  // Step 5 : Select phone number
  allocate_number(browser)
  // Step 5 : Select Contact to associate
  replace_contact(browser)
  // Step 6 : Edit Consents
  edit_consents(browser)
  
 })
  
  


it(testdata.TestCaseName8, function () {

  // Step 1 : Select a plan and click on activate now
  //openPlanListAndClickActivateNow(browser, testdata.PlanName[2])
  select_shake_and_remix(browser)
  /*
  browser.pause(10000)
  // Step 2 : Click on Checkout button
  click_continue_product_config(browser)
  // Step 2 : Click on Checkout button
  ClickonCheckout(browser)
  browser.pause(10000)
  // Step 3 : Insert Personal Information and Contact Information
  FillPersonalInfo(browser)
  // Step 4 : Personal Info And Contact Info
  create_address(browser)
  // Step 5 : Select phone number
  allocate_number(browser)
  // Step 5 : Select Contact to associate
  contact_continue(browser)
  // Step 6 : Edit Consents
  //edit_consents(browser)
 
 })

*/

afterEach(function ()  {
//browser.debug()
})

})