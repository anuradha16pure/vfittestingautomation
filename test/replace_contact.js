/** UI TEST VFIT */


const assert = require('chai').assert;
const expect = require('chai').expect;
CodiceFiscale = require('codice-fiscale-js/dist/codice.fiscale.commonjs2').CodiceFiscale
var testdata = require('../testdata/replace_contact.json')
var env_details = require('../testdata/env_config.json')


describe(testdata.TestSuite, function () {

  /**
   * Function opens plan list and clicks activateNow
   * possible to defer the end of the process using a promise.
   * @param {Object} exitCode 0 - success, 1 - fail
  */
 beforeEach(function () {
  console.log("Before each started");
  var caps = browser.session();
  console.log(browser.session('get').sessionId); // throws an error
  console.log("== ======================= ==");
  console.log("== Clean cache and Cookies ==");
  //openPlanList(browser);              
  //browser.sessionStorage('DELETE');
  browser.windowHandleFullscreen();
  console.log("Before each ended");
})


function close_banner(browser) {
  //Close banner
  //openEnv(browser)
  browser.waitForExist('#closeicon')
  browser.click('#closeicon')
  browser.pause(2000)
  
}

function openEnv(browser) {

  /*
  //Temparory login to crx/de

  browser.url('http://ilrtvit868:25050/crx/de')
  browser.click('#ext-gen228')
  browser.setValue("#ext-comp-1170",admin)
  browser.setValue("#ext-comp-1170",admin)
  browser.click('#ext-gen293')

  */
  //browser.debug()

  browser.url(env_details.ENVurl);
  //browser.url(link);

      console.log(" ============================");

      console.log("You are using env link - "+env_details.ENVurl);

      console.log(" ============================");

      
}

function openPlanListAndClickActivateNow(browser, planName) {
  browser.pause(25000)
  openEnv(browser)
  browser.pause(5000)
  close_banner(browser)

  browser.pause(2000)
 
  browser.scroll('//li[contains(.,"' + planName + '")]')
  
  browser.waitForExist('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
  
  browser.click('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
  
  browser.pause(10000)
 
}


function ClickonCheckout(browser) {
  browser.pause(10000)
  browser.waitForExist('//button[*="'+testdata.VerifyText[0].Checkout_Btn_Name+'"]')
  browser.click('//button[*="'+testdata.VerifyText[0].Checkout_Btn_Name+'"]')

}

function randomText() {
  var text = "";
  //Here vowels are taken into possible just to make the generation of fiscal code easier.
  var possible = "BCDFGHJKLMNPQRSTVWXYZ";
  for (var i = 0; i < 3; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}



var firstName = randomText();
var secondName = randomText();

var firstName_1 = randomText();
var secondName_1 = randomText();

function createCustomerWithFicscalCode() {
  var un = new Date().getTime()
  var cf = new CodiceFiscale({
    name: firstName,
    surname: secondName,
    gender: "M",
    day: 24,
    month: 7,
    year: 1957,
    birthplace: "Francia",
    birthplaceProvincia: "EE"
  });
  console.log("== createCustomerWithFicscalCode " + un + " ==");
  return cf;
}
function createContactWithFicscalCode() {
  var un = new Date().getTime()
  var cf = new CodiceFiscale({
    name: firstName_1,
    surname: secondName_1,
    gender: "F",
    day: 24,
    month: 7,
    year: 1980,
    birthplace: "Francia",
    birthplaceProvincia: "EE"
  });
  console.log("== createContactWithFicscalCode " + un + " ==");
  return cf;
}

function FillPersonalInfo(browser) {

  var cf = createCustomerWithFicscalCode()
  var document_num = cf.code;
  //browser.debug()
  browser.waitForExist('//input[@name="owningIndividual.firstName"]')
  var Pri_contact_name = cf.name + ' '+cf.surname;
  console.log(Pri_contact_name)
  //Set first name
  browser.setValue("input[name='owningIndividual.firstName']", cf.name)
  
  //Set last name
  browser.setValue("input[name='owningIndividual.lastName']", cf.surname)
  
  browser.keys("\t")
  
  browser.pause(2000)
  
  console.log(browser.getText('li[value="IT"]'))
  browser.click('li[value="IT"]')
  
  browser.keys("\t")
  //Set fiscal code
  browser.setValue("input[name='owningIndividual.fiscalCode']", cf.code)
  browser.scroll('input[name="owningIndividual.identification.identificationNumber"]')
  browser.keys("\t\t\t\t")
  browser.pause(10000)
  // Set Issuing nation
  browser.waitForExist('li[value="'+testdata.VerifyText[0].Document_dropdown_value+'"]')
  console.log(browser.getText('li[value="'+testdata.VerifyText[0].Document_dropdown_value+'"]'))
  browser.click('li[value="'+testdata.VerifyText[0].Document_dropdown_value+'"]')
  // Set value in document number
  browser.setValue("input[name='owningIndividual.identification.identificationNumber']", document_num)
  browser.keys("\t")
  browser.pause(2000)
  console.log(browser.getText('//div[section[input[@name="owningIndividual.identification.nationality"]]]//li[@value="IT"]'))
  browser.click('//div[section[input[@name="owningIndividual.identification.nationality"]]]//li[@value="IT"]')
  browser.keys("\t")
  //Because of a defect not setting default date remove it after the defect fix
  //browser.keys("Delete")
  browser.setValue("//div[@class='react-datepicker__input-container']/input","2020-04-10").keys("\t")

  // Click on continue
  browser.click('//div[@class="ds-create-customer ds-container"]//button[contains(.,"Continua")]')
  // Enter contact info
  browser.pause(5000)
  // set contact email id
  // browser.scroll('input[name="owningIndividual.email.emailAddress"]')
  browser.setValue("input[name='owningIndividual.email.emailAddress']", "anu@gmail.com").keys("\t")
  // confirm email id is now removed in 0.7
  // browser.setValue("input[name='owningIndividual.email.confirmEmailAddress']", "anu@gmail.com").keys("\t")
  // Enter phone number
  browser.setValue("input[name='owningIndividual.phone.phoneNumber']", "335123456")
  browser.keys("\t")
  // set preferred contact method
  browser.click('li[value="'+testdata.VerifyText[0].preferred_contact_method+'"]')
  browser.keys("\t")
  // set preferred contact time
  browser.click('li[value="'+testdata.VerifyText[0].preferred_contact_time+'"]')
  // leave preferred language as it is preselected as Italian
  
}

function create_address(browser) {

    
  //Step 4 : Enter city
  browser.scroll('//section[@class="address"]')
  browser.setValue('//section[@class="address"]/div[2]//div[1]/div[1]//div[@class="autocomplete-container ds-search"]/input',testdata.VerifyText[0].City_search_text)
  browser.pause(1000)
  // Select 1st from auto-complete options
  browser.waitForExist('//ul[@class="values-list generic-list"]')
  browser.click('//section[@class="address"]/div[2]//div[1]/div[1]//div[@class="autocomplete-container ds-search active"]//ul/li[1]')
  browser.pause(1000)
  // Step 4 : Enter street name
  browser.setValue('//section[@class="address"]/div[2]//div[1]/div[2]//div[@class="autocomplete-container ds-search"]/input',testdata.VerifyText[0].Street_search_text)
  browser.pause(25000)
  // Select 1st from auto-complete options
  browser.waitForExist('//ul[@class="values-list generic-list"]')
  browser.click('//ul[@class="values-list generic-list"]//li[1]')
  // Set house number
  browser.setValue("input[name='address.streetNumber']", "99")
  /*
  // Set value in mailbox
  browser.setValue("input[name='address.mailBox']","mailbox")
  // Set value in frazione
  browser.setValue("input[name='address.frazione']","frazione")
  */
  // Step 5 : Click on continue
  browser.pause(10000)
  browser.click('//button[contains(.,"Continua")]')
  browser.pause(40000)
  //browser.waitForExist('//span[contains(.,"edit")]')
  // browser.click('//span[contains(.,"edit")]')
}

function randomPhoneNum() {
  var phoneNumber = "35";
  var possible = "25154565";
  for (var i = 0; i < 8; i++)
  phoneNumber += possible.charAt(Math.floor(Math.random() * possible.length));
  return phoneNumber;
}

function set_PhoneNumber (browser) {

  // If port in selected 

  if(testdata.VerifyText[0].want_to_portin = "yes"){

    console.log('you are in port in loop')

    //click on keep my number port in button
    //browser.click('//input[@id="portInOption1_1"]') // //label[@for='portInOption1_1']

    browser.click('//label[@for="portInOption1_1"]') 
    browser.scroll('//label[@for="portInOption1_1"]')
    var portInMSISDN = randomPhoneNum()
    console.log(portInMSISDN)
    //console.log(portInMSISDN)
    //Enter current MSISDN
    browser.setValue("//input[@id='phoneNumber']",portInMSISDN).keys('\t')
    //Click on service provider dropdown
    //browser.click('//input[@name="currentProvider"]')
    //select your service provider
    browser.click('//li[contains(.,"'+testdata.VerifyText[0].Service_provider+'")]')
    //Set sim number else check the checkbox
    if(testdata.VerifyText[0].haveSIMnumber = "yes"){
    browser.setValue("//input[@id='simNumber']",testdata.VerifyText[0].SIM_number)
    }
    else{
      //browser.scroll('//input[@id="phoneNumber"]')
      browser.click('//input[@name="simAvailable"]')
    }
    //Click on radio button of subscription / Rechargable
    //input[@id='Subscription_0'] Rechargeable
    if(testdata.VerifyText[0].click_subscription_rechagable = "Subscription_0"){
      browser.click('//label[@for="'+testdata.VerifyText[0].click_subscription_rechagable+'"]')
    }
    else{
      browser.click('//label[@for="'+testdata.VerifyText[0].click_subscription_rechagable+'"]')
      if(testdata.VerifyText[0].rechargable_checkbox_value = "checked"){
      
      }
  
      else {
        browser.click('//input[@name="transferRemainingBalance"]')
      }
    }
     //Click on continue 
     browser.click('//button[contains(.,"Continua")]')
  }

  // If user wants new vodafone number
  else{

    // Step 6 : Phone Number Section 
    browser.waitForExist('//label[@id="portInOption2_0"]/span[@class="ds-form__icon"]')
    //Check by default your number is... radio button selected
    //Click on select new voda number radio button
    browser.waitForExist('//label[@for="retainVodafoneNumber_0"]//span[@class="ds-form__design"]|before')
    //if he wants to select from default numbers
    if(testdata.VerifyText[0].select_first_default_number = "yes"){
        
      browser.click('//label[@for="newVodafoneNumber_1"]')
      browser.waitForExist('//div[@class="ds-title__small"]')
      //Click on the first number out of 6 numbers
      browser.click('//li['+testdata.VerifyText[0].select_nth_default_number +']//input[@name= "phoneNumber"]')
    }
    //if he wants to select default numbers
    else{

    //Click on continue 
    browser.click('//button[contains(.,"Continua")]')
    
    }

  }
  //verify successfully submitted
  browser.pause(20000)
  browser.scroll('//div[@class="ds-info-box review"]')
  browser.waitForExist('//span[@class="ds-icon ds-icon-phone-number"]')
  //Verify if temporary number is being allocated.
  browser.waitForExist('//div[@class="note-text"]')
}

function replace_contact (browser) {
browser.waitForExist('//button[contains(.,"Modifica contatto")]')
//Click on replace contact
browser.click('//button[contains(.,"Modifica contatto")]')
browser.pause(2000)
//Click on Add Contact
browser.waitForExist('//span[contains(.,"Associa un contatto al numero")]')
browser.click('//span[contains(.,"Aggiungi un nuovo contatto")]')
browser.pause(10000)
create_contact(browser)
//Click on Save
browser.pause(5000)
//Click on Continue for replace contact
browser.click('//button[contains(.,"Salva")]')
var cf_1 = createContactWithFicscalCode()
var contact_name = cf_1.name + ' '+cf_1.surname;
console.log(contact_name)
//browser.debug()
assert.strictEqual(browser.getText('//span[@class="ds-title__small text-capitalize"]'),contact_name)
browser.click('//button[contains(.,"Continua")]')
browser.pause(10000)
browser.waitForExist('//div[@class="ds-title"]//span[@class="ds-icon ds-icon-contact"]')

}


function replace_contact_existing (browser){

  browser.waitForExist('//button[contains(.,"Modifica contatto")]')
  //Click on replace contact
  browser.click('//button[contains(.,"Modifica contatto")]')
  browser.pause(2000)
  //Click on Add Contact
  browser.waitForExist('//span[contains(.,"Associa un contatto al numero")]')
  browser.pause(2000)
  browser.click('//span[contains(.,"Aggiungi un nuovo contatto")]')
  browser.pause(10000)
  //create_contact(browser)

  //Using the default contact created in every env
  //browser.debug()
  var existing_firstname = "Testing";
  var existing_lastname = "Testing";
  var existing_fiscalename = "TSTTTN91B289999N";


  browser.waitForExist('//input[@name="firstName"]')
  //Set first name
  browser.setValue("input[name='firstName']", existing_firstname)
  
  //Set last name
  browser.setValue("input[name='lastName']", existing_lastname)
  
  browser.keys("\t")
  
  browser.pause(2000)
  
  console.log(browser.getText('li[value="IT"]'))
  browser.click('li[value="IT"]')
  
  browser.keys("\t")
  //Set fiscal code
  browser.setValue("input[name='fiscalCode']", existing_fiscalename).keys('\t')
  browser.pause(2000)
  //browser.debug()
  // check if exiting contact pop up is displayed
  var iscontact_existing = browser.waitForExist('//header//span[contains(.,"Is this an existing contact?")]');
  if(iscontact_existing == true)
  {
    browser.pause(1000)
    //Click on continue on existing contact pop over
    browser.click('//button[contains(.,"Yes, verify by SMS")]')
    //Click on continue on OTP pop over -- > No functionality as part  of MTV469
    browser.waitForExist('//header[contains(.,"Verifica informazioni")]')
    browser.click('//footer//button[contains(.,"Continua")]')

    //Click on ADD 
    browser.pause(7000)
    browser.debug()
    browser.waitForExist('//button[@class="ds-btn ds-btn--large ds-btn--primary"]//span[contains(.,"Aggiungi")]')
    browser.click('//button[@class="ds-btn ds-btn--large ds-btn--primary"]//span[contains(.,"Aggiungi")]')
    //Click on Save
    browser.pause(5000)
    //Click on Continue for replace contact
    browser.click('//button[contains(.,"Salva")]')
    var cf_1 = createContactWithFicscalCode()
    var contact_name = existing_firstname +" "+existing_lastname;
    console.log(contact_name)
    //browser.debug()
    assert.strictEqual(browser.getText('//span[@class="ds-title__small text-capitalize"]'),contact_name)
    browser.click('//button[contains(.,"Continua")]')
    browser.pause(10000)
    browser.waitForExist('//div[@class="ds-title"]//span[@class="ds-icon ds-icon-contact"]')

  }
 
  else{
    create_contact(browser)

    //Click on Save
    browser.pause(5000)
    //Click on Continue for replace contact
    browser.click('//button[contains(.,"Salva")]')
    var cf_1 = createContactWithFicscalCode()
    var contact_name = cf_1.name +" "+cf_1.surname;
    console.log(contact_name)
    //browser.debug()
    assert.strictEqual(browser.getText('//span[@class="ds-title__small text-capitalize"]'),contact_name)
    browser.click('//button[contains(.,"Continua")]')
    browser.pause(10000)
    browser.waitForExist('//div[@class="ds-title"]//span[@class="ds-icon ds-icon-contact"]')

  }


  
  


}

function create_contact (browser) {

  
  var cf_1 = createContactWithFicscalCode()
  var document_num = cf_1.code;
  var Sec_contact_name = cf_1.name + ' '+cf_1.surname;
  console.log(Sec_contact_name)
  //browser.debug()
  browser.waitForExist('//input[@name="firstName"]')
  //Set first name
  browser.setValue("input[name='firstName']", cf_1.name)
  
  //Set last name
  browser.setValue("input[name='lastName']", cf_1.surname)
  
  browser.keys("\t")
  
  browser.pause(2000)
  
  console.log(browser.getText('li[value="IT"]'))
  browser.click('li[value="IT"]')
  
  browser.keys("\t")
  //Set fiscal code
  browser.setValue("input[name='fiscalCode']", cf_1.code)
  browser.scroll('input[name="identification.identificationNumber"]')
  browser.keys("\t\t\t\t")
  browser.pause(2000)
  // Set Issuing nation
  browser.waitForExist('li[value="'+testdata.VerifyText[0].Document_dropdown_value+'"]')
  console.log(browser.getText('li[value="'+testdata.VerifyText[0].Document_dropdown_value+'"]'))
  browser.click('li[value="'+testdata.VerifyText[0].Document_dropdown_value+'"]')
  // Set value in document number
  browser.setValue("input[name='identification.identificationNumber']", document_num)
  browser.keys("\t")
  browser.pause(2000)
  console.log(browser.getText('//div[section[input[@name="identification.nationality"]]]//li[@value="IT"]'))
  browser.click('//div[section[input[@name="identification.nationality"]]]//li[@value="IT"]')
  browser.keys("\t")
  //Because of a defect not setting default date remove it after the defect fix
  //browser.keys("Delete")
  browser.setValue("//div[@class='react-datepicker__input-container']/input","2020-04-10")
  browser.keys("\t")
  // Enter contact info
  browser.pause(5000)
  // set contact email id
  // browser.scroll('input[name="owningIndividual.email.emailAddress"]')  
  browser.setValue("input[name='emailAddress']", "anu@gmail.com").keys("\t")
  
  // confirm email id is now removed in 0.7
  // browser.setValue("input[name='owningIndividual.email.confirmEmailAddress']", "anu@gmail.com").keys("\t")
  // Enter phone number
  browser.setValue("input[name='phoneNumber']", "335123445")
  browser.debug()
  /*
  browser.keys("\t")
  //browser.scroll('li[value="'+testdata.VerifyText[0].preferred_contact_method+'"]')
  // set preferred contact method
  browser.click('li[value="'+testdata.VerifyText[0].preferred_contact_method+'"]')
  browser.keys("\t")
  // set preferred contact time
  browser.click('li[value="'+testdata.VerifyText[0].preferred_contact_time+'"]')
  // leave preferred language as it is preselected as Italian
  */
  
  //Click on ADD 
  browser.pause(1000)
  browser.click('//button[@class="ds-btn ds-btn--large ds-btn--primary"]/span[contains(.,"Aggiungi")]')
  browser.pause(1000)

  return cf_1;
}

function  edit_consents(browser){
  
  browser.debug()
  
  browser.scroll('//header//span[@class="ds-title__text"]/span[contains(.,"Pagamento")]')

  browser.waitForExist('//span[contains(.,"Gestisci i tuoi consensi")]')

  for (let index = 1; index < 5; index++) {
    const element = index;
    
    browser.click('//div[@class="consents"]//label[@for="'+element+'--yes_0"]')
    browser.click('//div[@class="consents"]//label[@for="1--yes_0"]')
  }
  //Open I agree collapse
  browser.click('//button[@class="ds-open"]')
  //Click i agree checkbox
  browser.click('//label[@for="termsAndConditionsAcceptance"]')
  //click pay
  browser.click('//div[@class="btn-wrapper"]/button')
  browser.pause(5000)

  //wait for existance
  browser.waitForExist('//div[@class="ds-order-confirmation__item__content__icon__mail"]')

  }

//div[@class="consents"]//input[@id="2--yes_0"]
  



/*
 //******** test flow replace contact
 it(testdata.TestCaseName2, function () {

  // Step 1 : Select a plan and click on activate now
  openPlanListAndClickActivateNow(browser, testdata.VerifyText[0].PlanName)
  browser.pause(25000)
  // Step 2 : Click on Checkout button
  ClickonCheckout(browser)
  browser.pause(25000)
  // Step 3 : Insert Personal Information and Contact Information
  FillPersonalInfo(browser)
  // Step 4 : Personal Info And Contact Info
  create_address(browser)
  // Step 5 : Select phone number
  //set_PhoneNumber(browser)
  //Because of a defect
  //browser.debug()
  
  browser.pause(10000)
  //browser.debug()
  browser.click('//button[contains(.,"Continua")]')
  browser.pause(20000)
  //Step 6 : Replace Contact
  replace_contact(browser)
  //Verify Contact Created


  
  
})
*/



 //******** test flow portIn
 it(testdata.TestCaseName2, function () {
  /*
  // Step 1 : Select a plan and click on activate now
  openPlanListAndClickActivateNow(browser, testdata.VerifyText[0].PlanName)
  browser.pause(25000)
  // Step 2 : Click on Checkout button
  ClickonCheckout(browser)
  //browser.pause(25000)
  // Step 3 : Insert Personal Information and Contact Information
  FillPersonalInfo(browser)
  // Step 4 : Personal Info And Contact Info
  create_address(browser)
  //browser.debug()
  
  //browser.debug()
  // Step 5 : Select phone number
  //set_PhoneNumber(browser)
  //Because of a defect
  
  browser.pause(4000)
  //Click on continue 
  browser.click('//button[contains(.,"Continua")]')
  browser.pause(4000)
  */
  // Verify edit button displayed.

  browser.debug()

  //Step 6 : Replace Contact
  //replace_contact_existing(browser)
  //Verify Contact Created

  //Step 7 edit consents
  //browser.debug()

  edit_consents(browser)

  
  
})


 //******** test flow consents 
 it(testdata.TestCaseName2, function () {

  // Step 1 : Select a plan and click on activate now
  openPlanListAndClickActivateNow(browser, testdata.VerifyText[0].PlanName)
  browser.pause(25000)
  // Step 2 : Click on Checkout button
  ClickonCheckout(browser)
  browser.pause(25000)
  // Step 3 : Insert Personal Information and Contact Information
  FillPersonalInfo(browser)
  // Step 4 : Personal Info And Contact Info
  create_address(browser)
  // Step 5 : Select phone number
  //set_PhoneNumber(browser)
  //Because of a defect
  //browser.debug()
  
  browser.pause(10000)
  //browser.debug()
  browser.click('//button[contains(.,"Continua")]')
  browser.pause(20000)
  //Step 6 : Replace Contact
  //replace_contact(browser)
  //Verify Contact Created
  browser.click('//button[contains(.,"Continua")]')

  browser.debug()
  edit_consents(browser)
  
})

  


afterEach(function ()  {
browser.debug()
})

})
