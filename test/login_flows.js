/** UI TEST VFIT */


const assert = require('chai').assert;
const expect = require('chai').expect;
CodiceFiscale = require('codice-fiscale-js/dist/codice.fiscale.commonjs2').CodiceFiscale
var testdata = require('../testdata/login_flows.json')
var env_details = require('../testdata/env_config.json')


describe(testdata.TestSuite, function () {

  /**
   * Function opens plan list and clicks activateNow
   * possible to defer the end of the process using a promise.
   * @param {Object} exitCode 0 - success, 1 - fail
  */

 beforeEach(function () {
  console.log("Before each started");
  var caps = browser.session();
  console.log(browser.session('get').sessionId); // throws an error
  console.log("== ======================= ==");
  console.log("== Clean cache and Cookies ==");
  //openPlanList(browser);              
  //browser.sessionStorage('DELETE');
  browser.windowHandleFullscreen();
  console.log("Before each ended");
  browser.pause(5000)
  openEnv(browser)
  browser.pause(15000)
  close_banner(browser)

})

function close_banner(browser) {
  //Close banner
  //openEnv(browser)
  browser.waitForExist('#closeicon')
  browser.click('#closeicon')
  browser.pause(2000)
  
}

function openEnv(browser) {

  browser.url(env_details.ENV_Planlist);
  
      console.log(" ============================");

      console.log("You are using env link - "+env_details.ENV_Planlist);

      console.log(" ============================");

      
}

function login(browser,ContactId,CustomerId){
  var ContactId = ContactId;
  var CustomerId = CustomerId;
  console.log(ContactId + CustomerId)
  browser.setValue("\\input[name='obSSOCookie']",ContactId)
  browser.setValue("\\input[name='ssoSessionId']",CustomerId)
  browser.click('\\select[name="page"]')
  browser.click('//option[contains(.,"NAVIGATE_TO_PLAN_LIST")]')
  browser.click('//button[contains(.,"Login")]')
  browser.pause(5000)
  browser.waitForExist('//span[@class= "ss-username"]')

}



function  edit_consents(browser){

  
    browser.scroll('//header//span[@class="ds-title__text"]/span[contains(.,"Pagamento")]')
  
    browser.waitForExist('//span[contains(.,"Gestisci i tuoi consensi")]')
  
    for (let index = 1; index < 5; index++) {
      const element = index;
      
      browser.click('//div[@class="consents"]//label[@for="'+element+'--yes_0"]')
      browser.click('//div[@class="consents"]//label[@for="1--yes_0"]')
    }
    //Open I agree collapse
    browser.click('//button[@class="ds-open"]')
    //Click i agree checkbox
    browser.click('//label[@for="termsAndConditionsAcceptance"]')
    browser.pause(1000)
    browser.waitForExist('//div[@class="btn-wrapper"]/button/span')
    //click pay
    browser.click('//div[@class="btn-wrapper"]/button/span')
    browser.pause(5000)
  
    //wait for existance
    browser.waitForExist('//div[@class="ds-order-confirmation__item__content__icon__mail"]')
  
}

function edit_consents_onwer(browser){

  browser.waitForExist('//span[contains(.,"Contatti")]')

  var loggedInUser = browser.getText('//span[@class= "ss-username"]')
  //Open the expand button for Privacy settings
  browser.click('//section[@class="ds-form__combo ds-form__combo--box"]')
  /*
  //select owner from dropdown
  browser.click('//li[@value="'+ testdata.LoginDetailsOwner[0].ContactId +'"]')
  browser.pause(2000)
  */
  browser.click('//div/img[@class="ico-open"]')
  browser.pause(5000)
  browser.waitForExist('//div[@id="subscription_0"]')
  browser.click('//div[@id="subscription_0"]')
  browser.pause(2000)
  browser.click('//div[@class="consents primary-sec open"]//span[contains(.,"Modifica")]')
  browser.pause(2000)

 //Edit VF consents
 for (let index = 0; index < 2; index++) {
  const element = index;
  
  browser.click('//div[@class="consents"]//label[@for="'+element+'--yes_0"]')
  let elem = $('//span[contains(.,"Chiudi")]')
  let isExisting = elem.isExisting()
  console.log(isExisting)
  console.log(element)
  if (isExisting == true){
  browser.click('//span[contains(.,"Chiudi")]')
  }
  else{

  }
}

for (let index = 3; index < 5; index++) {
  const element = index;
  
  browser.click('//div[@class="consents"]//label[@for="'+element+'--no_1"]')

  let elem = $('//span[contains(.,"Chiudi")]')
  let isExisting = elem.isExisting()
  console.log(isExisting)
  console.log(element)
  if (isExisting == true){
  browser.click('//span[contains(.,"Chiudi")]')
  }
   else{
    
    }
 
}


// Open opposition consents
browser.scroll('//div[@class="consents-all-edit"]/div[contains(.,"Opposizione ")]//img[@class="ico-open"]')
browser.click('//div[@class="consents-all-edit"]/div[contains(.,"Opposizione ")]//img[@class="ico-open"]')
browser.pause(2000)
//Edit opposition consents
for (let index = 5; index < 9; index++) {
  const element = index;
 
  browser.pause(1000)
  
  browser.click('//div[@class="consents"]//label[@for="'+element+'--yes_0"]')
  
  let elem1 = $('//span[contains(.,"Chiudi")]')
  let isExisting1 = elem1.isExisting()
  console.log(isExisting1)
  console.log(element)
  if (isExisting1 == true){
  browser.click('//span[contains(.,"Chiudi")]')
  }
  else{
    
  }

  

}


  browser.click('//span[contains(.,"Salva")]')

}  

function edit_consents_member(browser){
  browser.pause(2000)
  //Open the expand button for Privacy settings
  browser.click('//div/img[@class="ico-open"]')
  browser.pause(2000)
  // click edit
  browser.click('//div[@class="default-widgets-head"]//button[contains(.,"Modifica")]')
  browser.pause(2000)
  //Edit VF consents
  for (let index = 0; index < 2; index++) {
    const element = index;
    
    browser.click('//div[@class="consents"]//label[@for="'+element+'--yes_0"]')
    let elem = $('//span[contains(.,"Chiudi")]')
    let isExisting = elem.isExisting()
    console.log(isExisting)
    console.log(element)
    if (isExisting == true){
    browser.click('//span[contains(.,"Chiudi")]')
    }
    else{

    }
  }
  
  for (let index = 3; index < 5; index++) {
    const element = index;
    
    browser.click('//div[@class="consents"]//label[@for="'+element+'--no_1"]')

    let elem = $('//span[contains(.,"Chiudi")]')
    let isExisting = elem.isExisting()
    console.log(isExisting)
    console.log(element)
    if (isExisting == true){
    browser.click('//span[contains(.,"Chiudi")]')
    }
     else{
      
      }
   
  }


  // Open opposition consents
  browser.scroll('//div[@class="consents-all-edit"]/div[contains(.,"Opposizione ")]//img[@class="ico-open"]')
  browser.click('//div[@class="consents-all-edit"]/div[contains(.,"Opposizione ")]//img[@class="ico-open"]')
  browser.pause(2000)
  //Edit opposition consents
  for (let index = 5; index < 9; index++) {
    const element = index;
   
    browser.pause(1000)
    
    browser.click('//div[@class="consents"]//label[@for="'+element+'--yes_0"]')
    
    let elem1 = $('//span[contains(.,"Chiudi")]')
    let isExisting1 = elem1.isExisting()
    console.log(isExisting1)
    console.log(element)
    if (isExisting1 == true){
    browser.click('//span[contains(.,"Chiudi")]')
    }
    else{
      
    }
  
}

  browser.click('//span[contains(.,"Salva")]')

}  

function edit_consents_prospect(browser){
  

  var loggedInUser = browser.getText('//span[@class= "ss-username"]')
  //Open the expand button for Privacy settings
  browser.click('//section[@class="ds-form__combo ds-form__combo--box"]')
  /*
  //select owner from dropdown
  browser.click('//li[@value="'+ testdata.LoginDetailsProspect[0].ContactId +'"]')
  browser.pause(2000)
  
  */
  browser.click('//div/img[@class="ico-open"]')
  browser.pause(2000)
  // click edit
  browser.click('//div[@class="default-widgets-head"]//button[contains(.,"Modifica")]')
  browser.pause(2000)
  //Edit VF consents
  browser.click('//div[@class="consents"]//label[@for="0--yes_0"]')
  browser.pause(2000)
  browser.click('//span[contains(.,"Salva")]')
}

function verify_prepaid_balance_wdgt_dashboard(browser){
  //div[@data-policy-id="subscription-highlights"]/div[contains(.,"3870000105")] | div[contains(.,"vfcd vfcd")]
  //div[@data-policy-id="subscription-highlights"]/div[contains(.,"3870000110")] | button[contains(.,"refresh")]
  //div[@data-policy-id="subscription-highlights"]/div[contains(.,"3870000105")] | span[contains(.,"Aggiorna")]
  browser.pause(5000)
  //browser.debug()
  // Verify widget exists
  
  let elem = $('//div[@data-policy-id="subscription-highlights"]//div[contains(.,"'+testdata.LoginDetailsOwner[0].MSIDN1+'")]')
  let isExisting = elem.isExisting()
    if (isExisting == true){
        //click refresh
        //browser.click('//div[@data-policy-id="subscription-highlights"]//div[contains(.,"'+testdata.LoginDetailsOwner[0].MSIDN1+'")] | span[contains(.,"'+testdata.VerifyText[0].Recharge_btn_name+'")]/a')
        browser.debug()
        browser.pause(2000)
        //get new window handles
        var windowHandles = browser.windowHandles()
        console.log(windowHandles); 
        var window = windowHandles.value[1];
        /*
        var title = browser.window(window).getTitle();
        console.log(title +'anuradha');
        expect(testdata.VerifyText[0].recharge_page_title).to.contain.toString(title)
        */
        var window1 = windowHandles.value[0];
        browser.switchTab(window1);
        browser.pause(5000)
        browser.waitForExist('//div[@data-policy-id="subscription-highlights"]//div[contains(.,"'+testdata.LoginDetailsOwner[0].MSIDN1+'")]')
      }

    else{

      browser.isExisting('//div[@class="wallet-ballance-container"]//button[contains(.,"Aggiorna")]') //button[contains(.,"'+testdata.VerifyText[0].Refresh_btn_name+'")]')
    }

}

function verify_prepaid_balance_wdgt_myproduct(browser){
  //div[@data-policy-id="subscription-highlights"]/div[contains(.,"3870000105")] | div[contains(.,"vfcd vfcd")]
  //div[@data-policy-id="subscription-highlights"]/div[contains(.,"3870000110")] | button[contains(.,"refresh")]
  //div[@data-policy-id="subscription-highlights"]/div[contains(.,"3870000105")] | span[contains(.,"Aggiorna")]
  browser.pause(5000)
  //browser.debug()
  // Verify widget exists
  
  let elem = $('//div[@data-policy-id="subscription-highlights"]//div[contains(.,"'+testdata.LoginDetailsOwner[0].MSIDN1+'")]')
  let isExisting = elem.isExisting()
    if (isExisting == true){
        //click refresh
        //browser.click('//div[@data-policy-id="subscription-highlights"]//div[contains(.,"'+testdata.LoginDetailsOwner[0].MSIDN1+'")] | span[contains(.,"'+testdata.VerifyText[0].Recharge_btn_name+'")]/a')
        browser.debug()
        browser.pause(2000)
        //get new window handles
        var windowHandles = browser.windowHandles()
        console.log(windowHandles); 
        var window = windowHandles.value[1];
        /*
        var title = browser.window(window).getTitle();
        console.log(title +'anuradha');
        expect(testdata.VerifyText[0].recharge_page_title).to.contain.toString(title)
        */
        var window1 = windowHandles.value[0];
        browser.switchTab(window1);
        browser.pause(5000)
        browser.waitForExist('//div[@data-policy-id="subscription-highlights"]//div[contains(.,"'+testdata.LoginDetailsOwner[0].MSIDN1+'")]')
      }

    else{

      browser.isExisting('//div[@class="wallet-ballance-container"]//button[contains(.,"Aggiorna")]') //button[contains(.,"'+testdata.VerifyText[0].Refresh_btn_name+'")]')
    }

}

function setDateF(){
  var date = new Date();
  var dd = date.getDate();
  var mm = date.getMonth() + 1; //January is 0!

  var yyyy = date.getFullYear();
  if (dd < 10) {
  dd = '0' + dd;
  } 
  if (mm < 10) {
  mm = '0' + mm;
  } 
  var dt = dd + '/' + mm + '/' + yyyy;
  
   return dt;
  
}

function verify_orders_list_page (browser) {
  var FromDate = setDateF();
  
  //browser.debug()
  browser.pause(5000)
  browser.isExisting('//div[@class="ds-title"]//span[contains(.,"'+ testdata.VerifyText[0].order_list_pageTitle+'")]')
  //browser.isExisting('//div[@class="ds-title"]//span[contains(.,"'+ testdata.VerifyText[0].order_list_pageTitle+'")]')
  //var status = testdata.VerifyText[0].default_order_status; 
  //console.log(status)
  assert.strictEqual(browser.getValue('//input[@name="orderStatus"]'),"tutti")
  //browser.debug()
  var isExisting = browser.isExisting('//ul[@class="generic-list ds-list"]/li[1]');
  console.log(isExisting)
  var FromDate = setDateF();
  console.log(FromDate)
  if (isExisting == true){
    browser.debug()
    let elem = $('//ul[@class="generic-list ds-list"]/li[1]//span[contains(.,"Ordine n°: ")]')
    console.log(elem.getValue())
    ///browser.getText('//ul[@class="generic-list ds-list"]/li[1]//span[contains(.,"Ordine n°: ")]')//get order ID
    var OrderDate = browser.getText('//ul[@class="generic-list ds-list"]/li[1]//div[@class="order-table"]/div[1]//div[@class="order-data"]')
    console.log(browser.getText('//ul[@class="generic-list ds-list"]/li[1]//div[@class="order-table"]/div[1]//div[@class="order-data"]'))
    browser.isExisting('//ul[@class="generic-list ds-list"]/li[1]//button')//Verify details button is present
    var form = $('//ul[@class="generic-list ds-list"]/li[1]//button')
    var isdisabled = form.getAttribute('disabled')
    if (isdisabled==true){
     
    }
    //browser.setValue('',FromDate)
  }
  else {
  
  }

  browser.click('//section[@class="ds-form__combo ds-form__combo--box"]') ////section[@class="ds-form__combo ds-form__combo--box"]
  browser.click('//ul/li[@value="completed"]')
  browser.pause(5000)
  browser.isExisting('//div[@class="row"]//span[contains(.,"'+testdata.VerifyText[0].no_records_found+'")]')
  
  browser.debug()
  
  ////div[@class="row"]//span[contains(.,"Nessun ordine trovato")]

}

function openPlanListAndClickActivateNow(browser, planName) {

  browser.pause(2000)
 
  browser.scroll('//li[contains(.,"' + planName + '")]')
  
  browser.waitForExist('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
  
  browser.click('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
  
  browser.pause(10000)
 
}

function ClickonCheckout(browser) {
  browser.pause(10000)
  browser.waitForExist('//button[*="'+testdata.VerifyText[0].Checkout_Btn_Name+'"]')
  browser.click('//button[*="'+testdata.VerifyText[0].Checkout_Btn_Name+'"]')

}

/*
it(testdata.TestCaseName1, function () {

  
 // Step 1 : Login with owner or member or prospect profile
 login(browser,testdata.LoginDetailsOwner[0].ContactId,testdata.LoginDetailsOwner[0].CustomerId)
 
 // Step 2 : Click on My contacts from header

 browser.url(env_details.ENV +'private/my-vodafone/my-account/my-contacts.html')
 browser.debug()
 edit_consents_onwer(browser)


 
})


it(testdata.TestCaseName2, function () {

  
 // Step 1 : Login with owner or member or prospect profile
 login(browser,testdata.LoginDetailsMember[0].ContactId,testdata.LoginDetailsMember[0].CustomerId)
 
 // Step 2 : Click on My contacts from header

 browser.url(env_details.ENV +'private/my-vodafone/my-account/my-contacts.html')
 

 edit_consents_member(browser)


 
})



it(testdata.TestCaseName3, function () {

  //browser.debug()
  
 // Step 1 : Login with owner or member or prospect profile
 login(browser,testdata.LoginDetailsProspect[0].ContactId,testdata.LoginDetailsProspect[0].CustomerId)
 
 
 // Step 2 : Click on My contacts from header

 browser.url(env_details.ENV +'private/my-vodafone/my-account/my-contacts.html')
 

 edit_consents_prospect(browser)


 
})



it(testdata.TestCaseName4, function () {

  //browser.debug()
  
 // Step 1 : Login with owner or member or prospect profile
 login(browser,testdata.LoginDetailsOwner[0].ContactId,testdata.LoginDetailsOwner[0].CustomerId)
  
 
 // Step 2 : Click on My dashboard from header

 browser.url(env_details.ENV +'my-dashboard.html')
 

 verify_prepaid_balance_wdgt_dashboard(browser)

 // Step 3 : Click on My product from header
 

 browser.url(env_details.ENV +'my-products.html')
 

 verify_prepaid_balance_wdgt_myproduct(browser)
 
})



it(testdata.TestCaseName5, function () {

  //browser.debug()
  
 // Step 1 : Login with owner or member or prospect profile
 login(browser,testdata.LoginDetailsOwner[1].ContactId,testdata.LoginDetailsOwner[1].CustomerId)
  
 

 browser.url(env_details.ENV +'my-account/orders')
 

 verify_orders_list_page(browser)
 
})

*/



it(testdata.TestCaseName6, function () {

/*
//Step 1 : Select a plan and click on activate now
openPlanListAndClickActivateNow(browser, testdata.TestCaseName6Data[0].PlanName)
browser.pause(10000)
//Step 2 : Click on Checkout button
ClickonCheckout(browser)
browser.pause(10000)
*/

browser.debug()

browser.waitForExist('//input[@name="owningIndividual.firstName"]')

//Set first name
browser.setValue("input[name='owningIndividual.firstName']", testdata.TestCaseName6Data[0].Fname)

//Set last name
browser.setValue("input[name='owningIndividual.lastName']", testdata.TestCaseName6Data[0].Lname)

//Set fiscal code
browser.setValue("input[name='owningIndividual.fiscalCode']", testdata.TestCaseName6Data[0].FiscaleCode)

browser.keys("\t") 

browser.pause(25000)
//verify edit mode
browser.waitForExist('//span[@class="ds-icon ds-icon-personal--dark"]')
 
 
})

/*
it(testdata.TestCaseName7, function () {

  //Step 1 : Select a plan and click on activate now
  openPlanListAndClickActivateNow(browser, testdata.TestCaseName7Data[0].PlanName)
  //Step 2 : Click on Checkout button
  ClickonCheckout(browser)
  browser.pause(10000)
  
  browser.debug()
  
  browser.waitForExist('//input[@name="owningIndividual.firstName"]')
  
  //Set first name
  browser.setValue("input[name='owningIndividual.firstName']", testdata.TestCaseName7Data[0].Fname)
  
  //Set last name
  browser.setValue("input[name='owningIndividual.lastName']", testdata.TestCaseName7Data[0].Lname)

  //Click Continue
  browser.click('//button[contains(.,"Continua")]')

  browser.scroll('input[name="owningIndividual.phone.phoneNumber"]')
  //Set Phone Number
  browser.setValue("input[name='owningIndividual.phone.phoneNumber']", testdata.TestCaseName7Data[0].PhoneNumber)
  
  browser.keys("\t") 

  browser.pause(15000)
  //Click Continue
  browser.click('//button[contains(.,"Continua")]')
  
  browser.pause(25000)
  //verify edit mode
  browser.waitForExist('//span[@class="ds-icon ds-icon-personal--dark"]')
   
   
  })
  
  */

afterEach(function ()  {
  browser.debug()
  })
  
  })