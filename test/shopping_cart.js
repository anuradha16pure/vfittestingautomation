/** UI TEST VFIT */


const assert = require('chai').assert;
const expect = require('chai').expect;
CodiceFiscale = require('codice-fiscale-js/dist/codice.fiscale.commonjs2').CodiceFiscale
var testdata = require('../testdata/shopping_cart.json')
var env_details = require('../testdata/env_config.json')

describe(testdata.TestSuite, function () {

  /**
   * Function opens plan list and clicks activateNow
   * possible to defer the end of the process using a promise.
   * @param {Object} exitCode 0 - success, 1 - fail
  */

  function openEnv(browser) {

    browser.debug()
    browser.url(env_details.ENVurl);
    //browser.url(link);

        console.log(" ============================");

        console.log("You are using env link - "+env_details.ENVurl);

        console.log(" ============================");

        
  }

  function close_banner(browser){
    //Close banner
    //browser.debug()
    browser.pause(5000)
    browser.waitForVisible('#closeicon')
    browser.click('#closeicon')
    browser.pause(2000)
  }



 

  function ClickDiscoverNow(browser, planName) {

    //openEnv(browser)
    close_banner(browser)
    //browser.debug()
    
    //browser.waitForVisible('//section[@class="plan"]/section/div/ul/li[1]')
    browser.pause(2000)
    //browser.debug()
    browser.scroll('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
    
    browser.waitForExist('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Discover_More_Button_Name+'"]')
    
    browser.click('//section[@class="plan"]//li[contains(.,"'+planName+'")]//button[contains(.,"'+testdata.VerifyText[0].Discover_More_Button_Name+'")]')
    //browser.debug()
    
    //browser.debug()
    
    browser.pause(15000)

    

    
    
    
  }


  function Add_Add_Ons(Browser) {

    //close_banner(browser)

    browser.pause(15000)

    browser.waitForExist('//h1[contains(.,"'+testdata.VerifyText[0].On_top_of_your_plan+'")]')

    browser.scroll('//h1[contains(.,"'+testdata.VerifyText[0].On_top_of_your_plan+'")]')
    /*
    var is_addable ;
    is_addable = assert.strictEqual(browser.getText('//span[@class="ds-title__main"]/span'),testdata.VerifyText[0].is_addable)

    if(is_addable == true){
      browser.click('//div[@class="ds-addon--item"]/div[contains(.,'+testdata.VerifyText[0].ADD_On_Name+')]//button[contains(.,'+testdata.VerifyText[0].Click_On_ADD+')]')
    }

    */

   browser.click('//div[@class="ds-addon--item"]/div[contains(.,"'+testdata.VerifyText[0].ADD_On_Name+'")]//button[contains(.,"'+testdata.VerifyText[0].Click_On_ADD+'")]')

    browser.pause(5000)

    console.log('clicked Add')

    browser.pause(5000)

    
   
  }

  function shopping_cart(browser){

    // Close banner

    //close_banner(browser)

    //Remove Add on

    browser.click('//span[@class="ds-icon ds-icon-trash_blue"]')

    browser.pause(5000)

    // Click on the Remove button on the pop up

    browser.click('//button[contains(.,"'+testdata.VerifyText[0].remove+'")]')

    browser.pause(20000)

    browser.waitForExist('//span[@class="ds-icon ds-icon-trash"]')

    // Click on the empty cart icon

    browser.click('//span[@class="ds-icon ds-icon-trash"]')

    browser.pause(5000)

    // Click on the empty cart button on the pop up

    browser.click('//button[contains(.,"'+testdata.VerifyText[0].empty_cart+'")]')

    browser.pause(2000)

    // Verify that the empty cart image is displayed on the shopping cart page

    browser.waitForExist('//div[@class="ds-empty-cart__image"]')
  }
  
  function selectlineupitem(browser){

    browser.pause(10000)

    browser.waitForExist('//span[contains(.,"'+testdata.VerifyText[0].selectlineupitem+'")]')

    browser.click('//span[contains(.,"'+testdata.VerifyText[0].LineupItemName+'")]')
  }


  beforeEach(function () {
    
    console.log("Before each started");
    var caps = browser.session();
    console.log(browser.desiredCapabilities);
    
    console.log(browser.session('get').sessionId); // throws an error
    console.log("== ======================= ==");
    console.log("== Clean cache and Cookies ==");
    openPlanList(browser);
    
    //browser.deleteCookie()
    //browser.sessionStorage('DELETE');
    //browser.windowHandleSize({ width: 1300, height: 1000 });
    browser.windowHandleFullscreen();
    
    console.log("Before each ended");
  })
  


  

  //******** test flow
  it(testdata.TestCaseName, function () {

    // Step 1 : Select a plan and click on Discover now
    ClickDiscoverNow(browser, testdata.VerifyText[0].PlanName)
    browser.pause(25000)
    // Step 2 : Click on Checkout button
    Add_Add_Ons(browser)
    browser.pause(25000)
    // Step 3 : Click Activate Now
    browser.click('//span[contains(.,"Activate Now")]')

    browser.pause(5000)

    //close_banner(browser)

    // Step 4 : Click SEE CART Deatils on checkout step

    browser.click('//button[contains(.,"SEE MORE DETAILS")]')

    browser.pause(5000)
    // Step 4 : Test Shopping cart
    shopping_cart(browser)
    //browser.click('//span[@class="ds-icon ds-icon-trash_blue"]')
    
    
    
  })
  /*
  

  it(testdata.TestCaseName, function () {
    // Step 1 : Select a plan and click on Discover now
    openPlanListAndClickDiscoverNow(browser, testdata.VerifyText[0].PlanName1)

    // Step 2 : Select Line up items

    selectlineupitem(browser)

    // Step 3 : Click Activate Now
    browser.click('//span[contains(.,"Activate Now")]')

    browser.pause(15000)

    //close_banner(browser)

    // Step 4 : Verify that the same line up plan is selected on checkout page.

    assert.strictEqual(browser.getText('//li[contains(.,"'+testdata.VerifyText[0].LineupItemName+'")]'),testdata.VerifyText[0].LineupItemName)

  }
  

  
  )

  */
      
  

  afterEach(function ()  {
  //browser.debug()
  })
})

