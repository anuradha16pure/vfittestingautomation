/** UI TEST VFIT */


const assert = require('chai').assert;
const expect = require('chai').expect;
CodiceFiscale = require('codice-fiscale-js/dist/codice.fiscale.commonjs2').CodiceFiscale
var testdata = require('../testdata/create_address.json')
var env_details = require('../testdata/env_config.json')

describe(testdata.TestSuite, function () {

  /**
   * Function opens plan list and clicks activateNow
   * possible to defer the end of the process using a promise.
   * @param {Object} exitCode 0 - success, 1 - fail
  */

  function openEnv(browser) {
    
    /*
    //Temparory login to crx/de

    browser.url('http://ilrtvit868:25050/crx/de')
    browser.click('#ext-gen228')
    browser.setValue("#ext-comp-1170",admin)
    browser.setValue("#ext-comp-1170",admin)
    browser.click('#ext-gen293')

    */
    browser.debug()

    browser.url(env_details.ENVurl);
    //browser.url(link);

        console.log(" ============================");

        console.log("You are using env link - "+env_details.ENVurl);

        console.log(" ============================");

        
  }


  function close_banner(browser) {
    //Close banner
    //openEnv(browser)
    browser.waitForExist('#closeicon')
    browser.click('#closeicon')
    browser.pause(2000)
    
  }

 

  function openPlanListAndClickActivateNow(browser, planName) {

    close_banner(browser)
    //browser.debug()
    openEnv(browser)
    //browser.waitForVisible('//section[@class="plan"]/section/div/ul/li[1]')
    browser.pause(2000)
    //browser.debug()
    browser.scroll('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
    
    browser.waitForExist('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
    
    browser.click('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
    //browser.debug()
    
    //browser.debug()
    
    browser.pause(10000)
    
    browser.back();

    //close_banner(browser)

    browser.scroll('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
    //browser.debug()
    browser.waitForExist('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')

    browser.click('//li[contains(.,"' + planName + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')
    
    
  }

  function ClickonCheckout(browser) {
    browser.pause(30000)
    browser.waitForExist('//button[*="'+testdata.VerifyText[0].Checkout_Btn_Name+'"]')
    //close_banner(browser)
    browser.scroll('//button[*="'+testdata.VerifyText[0].Checkout_Btn_Name+'"]')
    browser.click('//button[*="'+testdata.VerifyText[0].Checkout_Btn_Name+'"]')

  }

  function randomText() {
    var text = "";
    //Here vowels are taken into possible just to make the generation of fiscal code easier.
    var possible = "BCDFGHJKLMNPQRSTVWXYZ";
    for (var i = 0; i < 3; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  function randomNum(){
    
    var number =  (Math.floor(Math.random() * 99999) + 9999);
    return number;
  }

  var firstName = randomText();
  var secondName = randomText();
  

  function createCustomerWithFicscalCode() {
    var un = new Date().getTime()
    var cf = new CodiceFiscale({
      name: firstName,
      surname: secondName,
      gender: "M",
      day: 24,
      month: 7,
      year: 1957,
      birthplace: "Francia",
      birthplaceProvincia: "EE"
    });
    console.log("== createCustomerWithFicscalCode " + un + " ==");
    return cf;
  }

  function FillPersonalInfo(browser) {

    var cf = createCustomerWithFicscalCode()
    var document_num = randomNum()
    //browser.debug()
    browser.waitForExist('//input[@name="owningIndividual.firstName"]')
    //Set first name
    browser.setValue("input[name='owningIndividual.firstName']", cf.name)
    //close_banner(browser)
    //Set last name
    browser.setValue("input[name='owningIndividual.lastName']", cf.surname)
    //browser.debug()
    browser.keys("\t")
    //browser.setValue("input[name='owningIndividual.identification.identificationNumber']", new Date().getTime())
    //browser.keys("\t")
    browser.pause(2000)
    //Set Nationality as Italy
    //browser.waitForExist('li[value="IT"]')
    console.log(browser.getText('li[value="IT"]'))
    browser.click('li[value="IT"]')
    //browser.debug()
    /*
    browser.click('input[name="owningIndividual.nationality"]')
    browser.keys("ArrowDown")
    browser.keys("ArrowDown")
    browser.keys("ArrowDown")
    browser.keys("ArrowDown")
    browser.keys("Enter")
    */
    browser.keys("\t")
    //Set fiscal code
    browser.setValue("input[name='owningIndividual.fiscalCode']", cf.code)
    browser.scroll('input[name="owningIndividual.identification.identificationNumber"]')
    browser.keys("\t\t\t\t")
    /*
    browser.click('input[name="owningIndividual.identification.identificationType"]')
    browser.keys("ArrowDown")
    browser.keys("ArrowDown")
    browser.keys("Enter")
    */
    browser.pause(2000)
    // Set Issuing nation
    browser.waitForExist('li[value="'+testdata.VerifyText[0].Nationality_dropdown_value+'"]')
    console.log(browser.getText('li[value="'+testdata.VerifyText[0].Nationality_dropdown_value+'"]'))
    browser.click('li[value="'+testdata.VerifyText[0].Nationality_dropdown_value+'"]')
    // Set value in document number
    browser.setValue("input[name='owningIndividual.identification.identificationNumber']", document_num)
    browser.keys("\t")
    console.log(browser.getText('li[value="IT"]'))
    browser.click('li[value="IT"]')
    /*
    // browser.pause(2000)
    // browser.waitForExist('//li[@value="IT"]')
    // console.log(browser.getText('//li[@value="IT"]'))
    // browser.click('input[name="owningIndividual.identification.nationality"]')
       browser.keys("ArrowDown")
       browser.keys("ArrowDown")
       browser.keys("ArrowDown")
       browser.keys("ArrowDown")
       browser.keys("Enter")
    */
    browser.pause(15000)
    // Click on continue
    browser.click('div[class="ds-create-customer ds-container-fluid"] > div > div > div > section > div > div > button')
    // Enter contact info
    browser.pause(5000)
    // set contact email id
    // browser.scroll('input[name="owningIndividual.email.emailAddress"]')
    browser.setValue("input[name='owningIndividual.email.emailAddress']", "anu@gmail.com").keys("\t")
    // confirm email id is now removed in 0.7
    // browser.setValue("input[name='owningIndividual.email.confirmEmailAddress']", "anu@gmail.com").keys("\t")
    // Enter phone number
    browser.setValue("input[name='owningIndividual.phone.phoneNumber']", "335123456")
    browser.keys("\t")
    // set preferred contact method
    browser.click('li[value="'+testdata.VerifyText[0].preferred_contact_method+'"]')
    browser.keys("\t")
    // set preferred contact time
    browser.click('li[value="'+testdata.VerifyText[0].preferred_contact_time+'"]')
    // leave preferred language as it is preselected as Italian
    
  }

  function create_address(browser) {

    
    //Step 4 : Enter city
    browser.scroll('//section[@class="address"]')
    browser.setValue('//section[@class="address"]/div[2]//div[1]/div[1]//div[@class="autocomplete-container ds-search"]/input',testdata.VerifyText[0].City_search_text)
    browser.pause(1000)
    // Select 1st from auto-complete options
    browser.waitForExist('//ul[@class="values-list generic-list"]')
    browser.click('//section[@class="address"]/div[2]//div[1]/div[1]//div[@class="autocomplete-container ds-search active"]//ul/li[1]')
    browser.pause(1000)
    // Step 4 : Enter street name
    browser.setValue('//section[@class="address"]/div[2]//div[1]/div[2]//div[@class="autocomplete-container ds-search"]/input',testdata.VerifyText[0].Street_search_text)
    browser.pause(15000)
    // Select 1st from auto-complete options
    browser.waitForExist('//ul[@class="values-list generic-list"]')
    browser.click('//ul[@class="values-list generic-list"]//li[1]')
    // Set house number
    browser.setValue("input[name='address.streetNumber']", "99")
    /*
    // Set value in mailbox
    browser.setValue("input[name='address.mailBox']","mailbox")
    // Set value in frazione
    browser.setValue("input[name='address.frazione']","frazione")
    */
    // Step 5 : Click on continue
    browser.click('//button[contains(.,"Continue")]')
    browser.pause(25000)
    browser.waitForExist('//span[contains(.,"edit")]')
    // browser.click('//span[contains(.,"edit")]')
  }

    
  function set_PhoneNumber (browser) {

    // Step 6 : Phone Number Section 
    browser.waitForExist('//label[@for="portInOption2_0"]/span[@class="ds-form__icon"]')
    //Check by default your number is... radio button selected
    //Click on select new voda number radio button
    browser.waitForExist('//label[@for="retainVodafoneNumber_0"]//span[@class="ds-form__design"]|before')
    browser.click('//label[@for="newVodafoneNumber_1"]')
    browser.waitForExist('//div[@class="ds-title__small"]')
    //Click on the first number out of 6 numbers
    browser.click('//li[1]//input[@name= "phoneNumber"]')
    //Click on continue 
    browser.click('//button[contains(.,"CONTINUE")]')
    browser.pause(15000)
    //Edit mode verify 
    browser.waitForExist('//div[@class="ds-create-customer"]/div/div[2]/span[2]')
    /*
    //browser.scroll('//span[@class="ds-icon ds-icon-phone-number"]')
    // Step 6 : Phone Number Section 
    // Verify the Titles
    //assert.strictEqual(browser.getText('//span[@class="ds-title__main"]/span'),testdata.VerifyText[0].Tab_Title)
    //assert.strictEqual(browser.getText('//span[@class="ds-title__small"]'),testdata.VerifyText[0].PlanName)
    //assert.strictEqual(browser.getText('//li[1]/label[@for="portInOption2_0"]//div[@class="ds-form__text"]/span'),testdata.VerifyText[0].New_Number_Btn_Name)
    //Click on new Vodafone Number
    //browser.click('//label[@for="portInOption2_0"]')
    //Check new Vodafone Number is clicked and highlighted
    browser.waitForVisible('//label[@for="portInOption2_0"]/span[@class="ds-form__icon"]')
    //Check by default your number is... radio button selected
    //Click on select new voda number radio button
    browser.waitForVisible('//label[@for="retainVodafoneNumber_0"]//span[@class="ds-form__design"]|before')
    browser.click('//label[@for="newVodafoneNumber_1"]')
    browser.waitForVisible('//div[@class="ds-title__small"]')
    //assert.strictEqual(browser.getText('//div[@class="ds-title__small"]'),testdata.VerifyText[0].Select_New_Voda_Num_Title)
    //assert.strictEqual(browser.getText('//span[@class="ds-btn ds-btn--link preferredno dropdown__tooltip  "]/a/span'),testdata.VerifyText[0].Specify_Preffered_Num_title)
    //Click on choose number with combination link
    browser.click('//span[@class="ds-btn ds-btn--link preferredno dropdown__tooltip  "]/a')
    //assert.strictEqual(browser.getText('//label[@class="ds-field__title"]/span'),testdata.VerifyText[0].Selection_Info_Text)
    //assert.strictEqual(browser.getText('//button[@class="ds-btn"]/span/span'),testdata.VerifyText[0].Show_Num_Btn_txt)
    //Search For Invalid Combination
    browser.setValue('//input[@name="phone"]',"#admin")
    browser.keys("\t")
    //assert.strictEqual(browser.getText('//div[@class="ds-notification__error--text"]'),testdata.VerifyText[0].Error_Msg_Invalid_Number)
    browser.setValue('//input[@name="phone"]',"#12")
    browser.keys("\t")
    //assert.strictEqual(browser.getText('//div[@class="ds-notification__error--text"]'),testdata.VerifyText[0].Error_Msg_Min_Number)
    browser.setValue('//input[@name="phone"]',"12345678")
    browser.keys("\t")
    //assert.strictEqual(browser.getText('//div[@class="ds-notification__error--text"]'),testdata.VerifyText[0].Error_Msg_Max_Number)
    browser.setValue('//input[@name="phone"]',"999")
    browser.click('//button[@class="ds-btn"]')//click on show numbers
    browser.waitForExist('//div[@class="ds-alert__wrapper ds-alert__error"]')
    //assert.strictEqual(browser.getText('//div[@class="ds-alert__wrapper ds-alert__error"]/div/div/div/div'),testdata.VerifyText[0].Error_Msg_Number_Not_Found_Txt)
    browser.setValue('//input[@name="phone"]',"000") 
    browser.click('//button[@class="ds-btn"]')
    //browser.waitForVisible('//ul/li[1]/label/input[@name="phoneNumber"]')
    browser.waitForVisible('//button[@disabled]')
    browser.click('//label[@for="retainVodafoneNumber_0"]')
    browser.waitForVisible('//div[@class="row"]/div/button/span["CONTINUE"]')

    */
  }


  
  beforeEach(function () {
    console.log("Before each started");
    var caps = browser.session();
    console.log(browser.session('get').sessionId); // throws an error
    console.log("== ======================= ==");
    console.log("== Clean cache and Cookies ==");
    //openPlanList(browser);              
    //browser.sessionStorage('DELETE');
    browser.windowHandleFullscreen();
    console.log("Before each ended");
  })
  
  /*

  //******** test flow
  it(testdata.TestCaseName1, function () {

    // Step 1 : Select a plan and click on activate now
    openPlanListAndClickActivateNow(browser, testdata.VerifyText[0].PlanName)
    browser.pause(25000)
    // Step 2 : Click on Checkout button
    ClickonCheckout(browser)
    browser.pause(25000)
    // Step 3 : Insert Personal Information and Contact Information
    FillPersonalInfo(browser)
    // Step 4 : Personal Info And Contact Info
    create_address(browser)
    // Step 5 : Select phone number
    set_PhoneNumber(browser)
    
  })

  */

  //******** test flow
  it(testdata.TestCaseName2, function () {

    // Step 1 : Select a plan and click on activate now
    openPlanListAndClickActivateNow(browser, testdata.VerifyText[0].PlanName)
    browser.pause(25000)
    // Step 2 : Click on Checkout button
    ClickonCheckout(browser)
    browser.pause(25000)
    // Step 3 : Insert Personal Information and Contact Information
    FillPersonalInfo(browser)
    // Step 4 : Personal Info And Contact Info
    create_address(browser)
    // Step 5 : Select phone number
    set_PhoneNumber(browser)
    
  })
    
  

  afterEach(function ()  {
  //browser.debug()
  })
})

