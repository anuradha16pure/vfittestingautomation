/** UI TEST VFIT */



const expect = require('chai').expect;
CodiceFiscale = require('codice-fiscale-js/dist/codice.fiscale.commonjs2').CodiceFiscale
var testdata = require('../testdata/compare_plan_list.json')
var env_details = require('../testdata/env_config.json')

describe(testdata.TestSuite, function () {

  /**
   * Function opens plan list and clicks activateNow
   * possible to defer the end of the process using a promise.
   * @param {Object} exitCode 0 - success, 1 - fail
  */

  function openPlanList(browser) {
    browser.debug()

    browser.url(env_details.ENVurl);
    //browser.url(link);

        console.log(" ============================");

        console.log("You are using env link - "+env_details.ENVurl);

        console.log(" ============================");

        
  }


  function close_banner(browser) {
    //Close banner
    browser.waitForVisible('#closeicon')
    browser.click('#closeicon')
    browser.pause(2000)
    
  }

  function check_compare_checkbox(browser, planName){

    var plannameme = planName
    browser.click('//li[contains(.,"' + plannameme + '")]//div/div[1]/div[2]/div/div')
  }

  function Compare_plans(browser, planName1, planName2, planName3) {

    close_banner(browser)
    
    
    console.log("Plan list Page")


    console.log("==Testing Plan list page layout")

      //Steps:
      //1. Anonymous Customer launches the Self-service portal(Digital).
      //Called in before function
      //openPlanList(browser)

      
      //*********** Test that plan list is displayed
      //2. User should be able to view the plan list.

      browser.waitForVisible('//section[@class="plan"]/section/div/ul/li[1]')
      
      console.log("Below plans are displayed...");

      console.log(browser.getText('//h3'))

      console.log(testdata.VerifyText[0].Activate_Now_Button_Name)

      //3. Verify that User has an ability to view plan list and activate the plans. 

      browser.waitForExist('//li[contains(.,"' + planName1 + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')

      // 4. Verify that user can see more about the plans. (Discover plans)

      browser.waitForExist('//li[contains(.,"' + planName2 + '")]//button[*="'+ testdata.VerifyText[0].Discover_More_Button_Name+'"]')
      
     

      //******** Compare check box is present

      //5. Verify that user can select plans for comparision (Compare check box).


      browser.scroll('//ul/li[1]//div[@class="generic-item__header"]')

      browser.pause(1000)


      //********* Select a plan 1 for comparision 

      browser.waitForVisible('//li[contains(.,"' + planName1 + '")]')

      check_compare_checkbox(browser,planName1)


      console.log("Added " +planName1+ " for comparision...")

       //check plan is displayed in footer
       browser.waitForVisible('//div[@class="UXFWidget"]//h2[contains(.,"' +planName1 + '")]')

       console.log(planName1+ " is displayed in the sticky footer")
 
      //check compare is disabled
      browser.waitForVisible('//div[@class="UXFWidget"]//button[@class="ds-btn ds-btn--small ds-btn--primary disabled"]')
 
      console.log(" Compare button is disabled")

      //********* Select a plan 2 for comparision 
      browser.pause(1000)

      check_compare_checkbox(browser,planName2)


      console.log("Added " +planName2+ " for comparision...")

      //check compare is enabled
      browser.waitForVisible('//div[@class="UXFWidget"]//button[@class="ds-btn ds-btn--small ds-btn--primary"]')

      console.log(" Compare button is enabled")

      //Remove plans from comparision
      check_compare_checkbox(browser,planName1)

      browser.pause(1000)

      check_compare_checkbox(browser,planName2)

      browser.pause(1000)

     //Verify that sticky footer disappears...

      var exist = browser.waitForExist('//div[@class="UXFWidget"]//button[@class="ds-btn ds-btn--small ds-btn--primary"]',500,true)
      if(exist = true){
        console.log(" Sticky footer not displayed")
      }

      
      

       //Add plans from comparision
     

       browser.pause(1000)

       check_compare_checkbox(browser,planName1)

      browser.pause(1000)

      check_compare_checkbox(browser,planName2)

      browser.pause(1000)

      // Click on compare button

      browser.click('//div[@class="UXFWidget"]//button[contains(.,"'+ testdata.VerifyText[0].Compare_Button_Name+'")]')

  }




  function compare_plan_page(browser,planName1,planName2,planName3) {

    //Verify user is redirected to Compare page

    browser.pause(10000)

    //close_banner(browser)

    browser.waitForExist('//div[@class="ds-compare-device__header"]')

    

    expect('//span[@class="ds-title__text"]/span').to.contain.toString( testdata.VerifyText[0].Compare_Plans_Page_Title)

    console.log("Compare page is displayed")


    // Verify Plans are displayed on Compare page

    browser.waitForExist('//ul[@class="device__list plan__list"]/li[contains(.,"' + planName1 + '")]')


    //Verify Plan 1 and plan 2 are getting compared

    browser.waitForExist('//ul[@class="device__list plan__list"]/li[contains(.,"' + planName2 + '")]')

    // Display Comparision parameters

    console.log('Plans are compared on basis of below parameters...')
    
    console.log(browser.getText('//div[@class="ds-row specs"]/div/div'))

    //Verify user has abilility to activate plan and discover more about plans on compare page...

    browser.waitForExist('//ul[@class="device__list plan__list"]/li[contains(.,"' + planName1 + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')


    browser.waitForExist('//ul[@class="device__list plan__list"]/li[contains(.,"' + planName2 + '")]//button[*="'+ testdata.VerifyText[0].Discover_More_Button_Name+'"]')



    //Verify that sticky header with plan names is displayed if user scrolls down

    browser.scroll('//ul[@class="device__list plan__list"]/li[contains(.,"' + planName1 + '")]//button[*="'+ testdata.VerifyText[0].Activate_Now_Button_Name+'"]')

    expect('//div[@class="ds-sticky-header"]//h2').to.contain.toString(planName1)

    expect('//div[@class="ds-sticky-header"]//h2').to.contain.toString(planName2)

    browser.scroll('//button[@class="ds-btn ds-btn--secondary ds-right"]')

    // Verify back to plans exists
    expect('//button[@class="ds-btn ds-btn--secondary ds-right"]/span').to.contain.toString(testdata.VerifyText[0].Back_To_Plans_Button_Name)

    // Click on black to plans and verify user is redirected back to plan list page

    browser.click('//button[@class="ds-btn ds-btn--secondary ds-right"]')

    //close_banner(browser)

    browser.pause(1000)


    browser.waitForVisible('//ul/li[1]//div[@class="generic-item__header"]')

    browser.scroll('//ul/li[1]//div[@class="generic-item__header"]')

    //browser.waitForExist('//ul[@class="device__list plan__list"]/li[contains(.,"' + planName1 + '")]')

    console.log("Exit compare plans")
  }
  

  


  
  beforeEach(function () {
    
    console.log("Before each started");
    var caps = browser.session();
    console.log(browser.desiredCapabilities);
    
    console.log(browser.session('get').sessionId); // throws an error
    console.log("== ======================= ==");
    console.log("== Clean cache and Cookies ==");
    openPlanList(browser);
    
    //browser.deleteCookie()
    //browser.sessionStorage('DELETE');
    //browser.windowHandleSize({ width: 1300, height: 1000 });
    browser.windowHandleFullscreen();
    
    console.log("Before each ended");
  })
  

  //******** test flow
  it(testdata.TestCaseName, function () {
    
    
    //planListSelectPlan 
    Compare_plans(browser,testdata.PlanName[0],testdata.PlanName[1],testdata.PlanName[2])

    compare_plan_page(browser,testdata.PlanName[0],testdata.PlanName[1],testdata.PlanName[2])
    
    
  })


    
  

  afterEach(function ()  {
    browser.debug()
  })
})

