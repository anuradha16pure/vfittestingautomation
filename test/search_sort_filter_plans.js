/** UI TEST VFIT */

const expect = require('chai').expect;
const assert = require('chai').assert;
CodiceFiscale = require('codice-fiscale-js/dist/codice.fiscale.commonjs2').CodiceFiscale
var testdata = require('../testdata/search_sort_filter_plans.json')
var env_details = require('../testdata/env_config.json')

describe(testdata.TestSuite, function () {

  /**
   * Function opens plan list and clicks activateNow
   * possible to defer the end of the process using a promise.
   * @param {Object} exitCode 0 - success, 1 - fail
  */

  function openPlanList(browser) {
    browser.debug()

    browser.url(env_details.ENVurl);
    //browser.url(link);

        console.log(" ============================");

        console.log("You are using env link - "+env_details.ENVurl);

        console.log(" ============================");

        
  }

  function close_banner(browser){
    //Close banner
    browser.waitForVisible('#closeicon')
    browser.click('#closeicon')
    browser.pause(2000)
  }

  
  function search_plans(browser,searchInput,planName){
    var searchplan = searchInput;
    var VerifyPlan = planName;

    close_banner(browser)

    
    browser.pause(2000)
    

    //Search is displayed

    browser.waitForExist('//div[@class="ds-search"]')
    browser.setValue('//div[@class="ds-search"]/div/input',searchplan)
    browser.scroll('//div[@class="ds-search"]')
    browser.pause(2000)

    //browser.debug()

    //browser.click('//div[@class="ds-search"]/div/input')
    browser.keys("Enter")
    browser.pause(2000)    
    browser.waitForVisible('//div[@class="UXFWidget"]//h3')
    browser.pause(2000)
    //browser.click('//div[@class="ds-search"]/button')
    
    //browser.back()
    
    //expect('//section[@class="plan"]//h3').to.contain.toString(VerifyPlan)
    assert.strictEqual(browser.getText('//section[@class="plan"]//h3'),VerifyPlan)
    console.log("Plan is searched")

    

    
  }

  
  function sort_plans(browser,SortBy, planName1,planName2, planName3){
    var sortName = SortBy;
    
    close_banner(browser)

    browser.scroll('//div[@class="ds-search"]')
  
    browser.waitForVisible('//div[@class="UXFWidget"]//h3')

    
    browser.pause(2000)
    
    browser.click('//option[contains(.,"Sort By")]')
    //browser.click('//div[@data-for="sort"]')

    console.log("sorting options are... ")

    console.log(browser.getText('//div[@class="ds-dropdown__list"]'))

    browser.click('//div[@class="ds-dropdown__list"]/div[contains(.,"'+sortName+'")]')
    browser.pause(2000)
    
    browser.waitForVisible('//div[@class="UXFWidget"]//h3')

    browser.pause(2000)
    
    assert.strictEqual(browser.getText('//select[@name="sort"]'),sortName)

    assert.strictEqual(browser.getText('//div[@class="ds-plans"]//li[1]//h3'),planName1)
    assert.strictEqual(browser.getText('//div[@class="ds-plans"]//li[2]//h3'),planName2)
    assert.strictEqual(browser.getText('//div[@class="ds-plans"]//li[3]//h3'),planName3)

  }

  

  

 function filter_plans(browser,filtername1, filtername2){
  //var searchplan = planName;

  close_banner(browser)

  browser.scroll('//div[@class="ds-search"]')

  browser.waitForVisible('//div[@class="UXFWidget"]//h3')

  browser.pause(2000)

  browser.click('//option[contains(.,"More Filters")]')
  //browser.debug()

  browser.click('//div[@class="ds-dropdown open"]/div[2]//label/span[contains(.,"'+filtername1+'")]')
  
  browser.click('//div[@class="ds-dropdown open"]/div[2]//label[contains(.,"'+filtername2+'")]')


  browser.click('//div[@class="ds-dropdown open"]/div[2]//button[contains(.,"Apply")]')
  browser.pause(2000)
  browser.waitForVisible('//div[@class="UXFWidget"]//h3')
  browser.pause(2000)
  assert.strictEqual(browser.getText('//div[@class="ds-plans"]//li[1]//h3'),testdata.PlanName[1])
  expect('//ul/li//li[contains(.,"'+filtername1+'")]').to.contain.toString(filtername1)
  expect('//ul/li//li[contains(.,"'+filtername2+'")]').to.contain.toString(filtername2)
  
  assert.strictEqual(browser.getText('//div[@class="ds-plans"]//li[2]//h3'),testdata.PlanName[2])
  assert.strictEqual(browser.getText('//ul/li[2]//li[contains(.,"'+filtername1+'")]'),filtername1)
  assert.strictEqual(browser.getText('//ul/l[2]]//li[contains(.,"'+filtername2+'")]'),filtername2)
  //console.log(browser.getText('//div[@class="UXFWidget"]//h3') + "is filtered out...")


  

 }
 
function openEnv(browser) {

  /*
  //Temparory login to crx/de

  browser.url('http://ilrtvit868:25050/crx/de')
  browser.click('#ext-gen228')
  browser.setValue("#ext-comp-1170",admin)
  browser.setValue("#ext-comp-1170",admin)
  browser.click('#ext-gen293')

  */
  browser.debug()

  browser.url(env_details.ENVurl);
  //browser.url(link);

      console.log(" ============================");

      console.log("You are using env link - "+env_details.ENVurl);

      console.log(" ============================");

      
}

 beforeEach(function () {
  console.log("Before each started");
  var caps = browser.session();
  console.log(browser.session('get').sessionId); // throws an error
  console.log("== ======================= ==");
  console.log("== Clean cache and Cookies ==");
  //openPlanList(browser);              
  //browser.sessionStorage('DELETE');
  browser.windowHandleFullscreen();
  console.log("Before each ended");
})

  //******** test flow
 
  
 
   it(testdata.TestCaseName[2], function () {
    browser.pause(2000)
    openEnv(browser)

    filter_plans(browser,testdata.FilterWithPara[0],testdata.FilterWithPara[1])

  })

 it(testdata.TestCaseName[0], function () {
    browser.pause(2000)
    openEnv(browser)
    sort_plans(browser,testdata.Sort_By[0],testdata.PlanName[0],testdata.PlanName[1],testdata.PlanName[2])
  
  })
  
  it(testdata.TestCaseName[1], function () {
    browser.pause(2000)
    openEnv(browser)
    search_plans(browser,testdata.searchInput[0],testdata.PlanName[0])
    
    
  })


  


 
  
  
  afterEach(function () {
    //browser.debug()
  })

})
