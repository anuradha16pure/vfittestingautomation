bin\pskill64 node
bin\pskill64 2.40-x64-chromedriver

cmd /c npm install 
rem installation part with proxy required
set http_proxy=http://genproxy.corp.amdocs.com:8080
set NODE_TLS_REJECT_UNAUTHORIZED=0
cmd /c npm run selenium:install
set http_proxy=

@rem start selenium
start cmd /c npm run selenium:start

@rem sleep 10 sec before selenium started
ping 127.0.0.1 -n 10 > nul

cmd /c npm test
pause