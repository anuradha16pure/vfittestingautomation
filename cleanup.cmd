@echo off

bin\pskill64 node
bin\pskill64 2.40-x64-chromedriver

set "CUST_ROOT=%cd%"

echo Cleaning %CUST_ROOT% ...

@rem create a empty foldre for robocopy deap clean if RD fails
rd /s /q %CUST_ROOT%\empty 2> nul
mkdir %CUST_ROOT%\empty

@rem cleanup logs from all subfolders
@rem for /r %%x in (*.log, dependency.txt, dependency_plugins.txt) do (
for /r %%x in (*.log, dependency.txt, .classpath, .project, yarn.lock, package.json.md5) do (
    IF EXIST %%x (
    	echo Cleanup file %%x
    	del /f %%x
 	)
)

FOR /R  %%G in (node_modules, target, .npm-cache, .settings, errorShots, test-reports) DO (
 	IF EXIST %%G (
 		@rem pushd %%G
 		echo Cleanup in %%G
    del /q/f  %%G  > nul 2>&1
		rd  /s/q  %%G  > nul 2>&1
		IF EXIST %%G robocopy /MIR %CUST_ROOT%\empty  %%G > nul 2>&1
 		@rem popd
 	)
)

@rem remove temporarty folder
rd /q /s %CUST_ROOT%\empty

pause
